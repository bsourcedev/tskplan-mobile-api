﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tskplan.Data;
using DTO = Tskplan.Models.Obra.DTO;

namespace Tskplan.Repositories.Obra
{
    public class ObraRepository : IObraRepository
    {
        private readonly IDapperDatabaseHelper _dapperDatabaseHelper;
        private readonly ILogger<ObraRepository> _logger;

        public ObraRepository(IDapperDatabaseHelper dapperDatabaseHelper, ILogger<ObraRepository> logger)
        {
            _dapperDatabaseHelper = dapperDatabaseHelper ?? throw new ArgumentNullException(nameof(dapperDatabaseHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<DTO.Entrega>, int>> Get(int empresaId, DateTime ? lastSync)
        {
            //todo altererar view para ter uma "data de atualização" e não utilizar mais o getdate
            string script = @"
                SELECT ObraId
                     , Nome
                     , Sigla
                     , CheckListAbertos    AS Pendentes
                     , CheckListRejeitados AS Rejeitados
                     , CheckListConcluidos AS Aprovados
                     , Guid                AS guid
                     , 1                   AS isSync
                     , 1                   AS isCreated
                     , 0                   AS isDeleted
                     , GETDATE()           AS syncDate  
                  FROM V_Mobile_Obra_Checklists
                 WHERE EmpresaId = @EmpresaId
                   AND (@LastSync IS NULL 
                    OR GETDATE() > @LastSync)
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.Entrega>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.ObraLocal>, int>> GetLocais(int obraId, int empresaId, DateTime? lastSync)
        {
            string script = @"
				  WITH LocaisChecklist AS (
				SELECT C.ObraLocalId
					 , COUNT (1) AS Total
				  FROM V_OrdemServico_Checklist C
				  JOIN V_Tarefa_Andamento		A ON C.ObraCronogramaTarefaId = A.ObraCronogramaTarefaId
                 WHERE C.EmpresaId = @EmpresaId
				   AND A.Porcentagem > 0
                   AND (@LastSync IS NULL 
                    OR C.DataHoraAlteracao > @LastSync)
			  GROUP BY ObraLocalId
					 )
				     , Locais AS (
	        	SELECT O.ObraLocalId
	        		 , ObraId
	        		 , Caminho
	        		 , CASE 
	             			WHEN LEN(replace(RIGHT(Caminho,charindex('\',reverse(Caminho),1)+0),'\',''))>0
	             				THEN replace(RIGHT(Caminho,charindex('\',reverse(Caminho),1)+0),'\','')
	             		    ELSE Caminho
	             	   END				  AS Nome
					 , O.Guid
					 , LC.Total
	        	  FROM ObraLocal	   O
			 LEFT JOIN LocaisChecklist LC ON O.ObraLocalId = LC.ObraLocalId
	        	 WHERE EmpresaId = @EmpresaId
	        	   AND (ObraId  = @ObraId
				    OR  @ObraId = 0)
	        	     )
	        		 , Hierarquia AS (
	        	SELECT L.* 
	        		 , P.ObraLocalId	AS PaiId
	        	  FROM Locais		L
	         LEFT JOIN ObraLocal	P	ON P.Caminho	  = REPLACE(L.Caminho ,CONCAT('\',L.Nome),'')
	        						   AND L.ObraId		  = P.ObraId
	        						   AND L.ObraLocalId != P.ObraLocalId
	        		 )
	        	SELECT ObraLocalId
                     , ObraId
                     , Caminho
                     , Nome
                     , PaiId
					 , Guid
					 , Total		AS QuantidadeChecklist
	        	  FROM Hierarquia H
				 WHERE (ISNULL(H.Total,0) > 0
				    OR H.ObraLocalId = (SELECT TOP 1 ISNULL(X.PaiId,0) FROM Hierarquia X WHERE X.PaiId = H.ObraLocalId))
			  ORDER BY Caminho
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                ObraId = obraId,
                EmpresaId = empresaId,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.ObraLocal>(script, dParams: dynamic, useTransaction: true);
        }
    }
}
