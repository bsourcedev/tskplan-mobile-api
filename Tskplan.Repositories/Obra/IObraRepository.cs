﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Obra.DTO;

namespace Tskplan.Repositories.Obra
{
    public interface IObraRepository
    {
        Task<Tuple<IEnumerable<DTO.Entrega>, int>> Get(int empresaId, DateTime ? lastSync);
        Task<Tuple<IEnumerable<DTO.ObraLocal>, int>> GetLocais(int obraId, int empresaId, DateTime? lastSync);
    }
}
