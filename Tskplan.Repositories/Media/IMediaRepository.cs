﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tskplan.Models.Media.ViewModel;
using DTO = Tskplan.Models.Media.DTO;

namespace Tskplan.Repositories.Media
{
    public interface IMediaRepository
    {
        Task<DTO.Media> UpdateImage(MediaPost item);
    }
}
