﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tskplan.Data;
using Tskplan.Models.Media.ViewModel;
using DTO = Tskplan.Models.Media.DTO;

namespace Tskplan.Repositories.Media
{
    public class MediaRepository: IMediaRepository
    {
        private readonly IDapperDatabaseHelper _dapperDatabaseHelper;
        private readonly ILogger<MediaRepository> _logger;

        public MediaRepository(IDapperDatabaseHelper dapperDatabaseHelper, ILogger<MediaRepository> logger)
        {
            _dapperDatabaseHelper = dapperDatabaseHelper ?? throw new ArgumentNullException(nameof(dapperDatabaseHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<DTO.Media> UpdateImage(MediaPost item)
        {
            string script = @"
                INSERT  
				  INTO Media
				     ( EmpresaId
					 , Guid
					 , Url
					 , Plataforma
                     , NomeArquivo
					 , Latitude
					 , Longitude
                     , Extensao
					 , UsuarioIdInclusao
					 , DataHoraInclusao)
			    SELECT @EmpresaId
					 , @Guid
					 , @Url
                     , 'M'
                     , @NomeArquivo
					 , @Latitude
					 , @Longitude
                     , @Extensao
					 , @UsuarioId
					 , GETDATE();

                SELECT EmpresaId
					 , MediaId
					 , Guid
					 , Url
                     , NomeArquivo AS Nome
					 , Plataforma
					 , Latitude
					 , Longitude
                     , Extensao
					 , UsuarioIdInclusao
					 , DataHoraInclusao
			      FROM Media
				 WHERE Guid = @Guid;";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                item.EmpresaId,
                item.Guid,
                item.Url,
                NomeArquivo = item.Nome,
                item.Latitude,
                item.Longitude,
                item.Extensao,
                item.UsuarioId
            });
            //
            return await _dapperDatabaseHelper.QueryOne<DTO.Media>(script, dParams: dynamic, useTransaction: true);
        }
    }
}
