﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Checklist.DTO;

namespace Tskplan.Repositories.Checklist
{
    public interface IChecklistRepository
    {
        Task<Tuple<IEnumerable<DTO.ChecklistLista>, int>> GetLista(int obraId, int obraLocalId, int tarefaId, int empresaId, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.Checklist>, int>> Get(int obraCronogramaTarefaLocalId, int obraCronogramaTarefaChecklistId, int empresaId, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.Checklist>, int>> GetById(int OrdemServicoCheckListId, int empresaId);
        Task<int> Put(DTO.Checklist checklist, int usuarioId, int empresaId);

        Task<Tuple<IEnumerable<DTO.Arquivo>, int>> Arquivos(int ObraCronogramaTarefaLocalId, int ObraCronogramaTarefaCheckListId, int empresaId, DateTime? lastSync);
    }
}
