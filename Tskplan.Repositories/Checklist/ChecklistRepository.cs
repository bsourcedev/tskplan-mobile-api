﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tskplan.Data;
using DTO = Tskplan.Models.Checklist.DTO;

namespace Tskplan.Repositories.Checklist
{
    public class ChecklistRepository : IChecklistRepository
    {
        private readonly IDapperDatabaseHelper _dapperDatabaseHelper;
        private readonly ILogger<ChecklistRepository> _logger;

        public ChecklistRepository(IDapperDatabaseHelper dapperDatabaseHelper, ILogger<ChecklistRepository> logger)
        {
            _dapperDatabaseHelper = dapperDatabaseHelper ?? throw new ArgumentNullException(nameof(dapperDatabaseHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<DTO.ChecklistLista>, int>> GetLista(int obraId, int obraLocalId, int tarefaId, int empresaId, DateTime? lastSync)
        {
            string script = @$"
		         DECLARE @ObraLocalId	INT = NULLIF({obraLocalId},0)
		         	   , @TarefaId		INT = NULLIF({tarefaId},0);
	  	  
					EXEC S_OrdemServicoCheckList_Guid @EmpresaId, @ObraId, @TarefaId;

					WITH OS AS (
				  SELECT OS.*
					   , T.ObraId
					   , T.Guid							AS TarefaGuid
				    FROM ObraCronogramaTarefaLocal	OS
				    JOIN ObraCronogramaTarefa		T	ON OS.ObraCronogramaTarefaId = T.ObraCronogramaTarefaId
					JOIN V_Tarefa_Andamento			A	ON T.ObraCronogramaTarefaId = A.ObraCronogramaTarefaId
				   WHERE T.EmpresaId = @EmpresaId
				     AND (T.ObraId = @ObraId
					  OR  @ObraId = 0)
					 AND Checked = 1
					 AND A.Porcentagem > 0
				     AND (@ObraLocalId IS NULL 
					  OR  OS.ObraLocalId = @ObraLocalId)
					   )
					   , Dados AS (
				  SELECT CONCAT(TCK.ObraCronogramaTarefaCheckListId,'-',OS.ObraCronogramaTarefaLocalId) AS Id
					   , OS.ObraCronogramaTarefaLocalId
					   , OS.ObraCronogramaTarefaId
					   , TCK.ObraCronogramaTarefaCheckListId
					   , OSCK.OrdemServicoCheckListId
					   , 'Checklist'											AS Titulo
					   , TCK.Item												AS Descricao
					   , ISNULL(OSCK.Situacao,-1)								AS StatusId
					   , OS.ObraId
					   , OS.ObraLocalId
					   , OS.ObraCronogramaTarefaId								AS TarefaId
					   , G.Guid
					   , OS.TarefaGuid
					   , ISNULL(OSCK.DataHoraAlteracao,OS.DataHoraAlteracao)	AS DataHoraAlteracao
				    FROM OS
					JOIN ObraCronogramaTarefaCheckList	TCK		ON OS.ObraCronogramaTarefaId = TCK.ObraCronogramaTarefaId
					JOIN OrdemServicoCheckListGuid      G		ON OS.ObraCronogramaTarefaLocalId = G.ObraCronogramaTarefaLocalId		
															   AND TCK.ObraCronogramaTarefaCheckListId = G.ObraCronogramaTarefaCheckListId
			   LEFT JOIN OrdemServicoCheckList			OSCK	ON TCK.ObraCronogramaTarefaCheckListId = OSCK.ObraCronogramaTarefaCheckListId
															   AND OSCK.ObraCronogramaTarefaLocalId = OS.ObraCronogramaTarefaLocalId
				   WHERE (@TarefaId IS NULL
					  OR  TCK.ObraCronogramaTarefaId = @TarefaId)
                     AND (@LastSync IS NULL 
                      OR  ISNULL(OSCK.DataHoraAlteracao, OS.DataHoraAlteracao) > @LastSync)
					   )
			      SELECT * 
					FROM Dados";

            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                ObraId = obraId,
                LastSync = lastSync
            });

            return await _dapperDatabaseHelper.QueryAll<DTO.ChecklistLista>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.Checklist>, int>> Get(int obraCronogramaTarefaLocalId, int obraCronogramaTarefaChecklistId, int empresaId, DateTime? lastSync)
        {
            string script = @$"
					WITH Dados AS (
				  SELECT CONCAT(TCK.ObraCronogramaTarefaCheckListId,'-',@ObraCronogramaTarefaLocalId) AS Id
					   , @ObraCronogramaTarefaLocalId							AS ObraCronogramaTarefaLocalId
					   , TCK.ObraCronogramaTarefaId
					   , TCK.ObraCronogramaTarefaCheckListId
					   , OSCK.OrdemServicoCheckListId
					   , 'Checklist'											AS Titulo
					   , TCK.Item												AS Descricao
					   , OSCK.Motivo
					   , OSCK.MotivoImagemGuid
					   , M.Url													AS MotivoImagemURL
					   , OSCK.DataInspecao
					   , TCK.QuantoConferir
					   , TCK.Visual
					   , TCK.Trena
					   , TCK.Projeto
					   , TCK.Nivel
					   , TCK.Prumo
					   , TCK.InformacaoComplementar
					   , TCK.Aceitacao
					   , TCK.Observacao
					   , ISNULL(OSCK.Situacao,-1)								AS StatusId
					   , G.Guid
					   , ISNULL(OSCK.DataHoraAlteracao, TCK.DataHoraAlteracao)	AS DataHoraAlteracao
				    FROM ObraCronogramaTarefaCheckList	TCK		
					JOIN OrdemServicoCheckListGuid      G		ON @ObraCronogramaTarefaLocalId = G.ObraCronogramaTarefaLocalId		
															   AND TCK.ObraCronogramaTarefaCheckListId = G.ObraCronogramaTarefaCheckListId
			   LEFT JOIN OrdemServicoCheckList			OSCK	ON TCK.ObraCronogramaTarefaCheckListId = OSCK.ObraCronogramaTarefaCheckListId
															   AND OSCK.ObraCronogramaTarefaLocalId = @ObraCronogramaTarefaLocalId
			   LEFT JOIN Media							M		ON OSCK.MotivoImagemGuid = M.Guid
				   WHERE TCK.EmpresaId = @EmpresaId
				     AND TCK.ObraCronogramaTarefaChecklistId = @ObraCronogramaTarefaChecklistId
                     AND (@LastSync IS NULL 
                      OR ISNULL(OSCK.DataHoraAlteracao, TCK.DataHoraAlteracao) > @LastSync)
					   )
			      SELECT * 
					FROM Dados ";

            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                ObraCronogramaTarefaLocalId = obraCronogramaTarefaLocalId,
                ObraCronogramaTarefaChecklistId = obraCronogramaTarefaChecklistId,
                LastSync = lastSync
            });


            return await _dapperDatabaseHelper.QueryAll<DTO.Checklist>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.Checklist>, int>> GetById(int OrdemServicoCheckListId, int empresaId)
        {
            string script = @$"
					WITH Dados AS (
				  SELECT CONCAT(TCK.ObraCronogramaTarefaCheckListId,'-',OSCK.ObraCronogramaTarefaLocalId) AS Id
					   , OSCK.ObraCronogramaTarefaLocalId						AS ObraCronogramaTarefaLocalId
					   , TCK.ObraCronogramaTarefaId
					   , TCK.ObraCronogramaTarefaCheckListId
					   , OSCK.OrdemServicoCheckListId
					   , 'Checklist'											AS Titulo
					   , TCK.Item												AS Descricao
					   , OSCK.Motivo
					   , OSCK.MotivoImagemGuid
					   , M.Url													AS MotivoImagemURL
					   , OSCK.DataInspecao
					   , TCK.QuantoConferir
					   , TCK.Visual
					   , TCK.Trena
					   , TCK.Projeto
					   , TCK.Nivel
					   , TCK.Prumo
					   , TCK.InformacaoComplementar
					   , TCK.Aceitacao
					   , TCK.Observacao
					   , ISNULL(OSCK.Situacao,-1)								AS StatusId
					   , OSCKG.GUID
					   , ISNULL(OSCK.DataHoraAlteracao, TCK.DataHoraAlteracao)	AS DataHoraAlteracao
				    FROM OrdemServicoCheckList			OSCK
					JOIN OrdemServicoCheckListGuid		OSCKG	ON OSCK.ObraCronogramaTarefaLocalId = OSCKG.ObraCronogramaTarefaLocalId
															   AND OSCK.ObraCronogramaTarefaCheckListId = OSCKG.ObraCronogramaTarefaCheckListId
				    JOIN ObraCronogramaTarefaCheckList	TCK		ON TCK.ObraCronogramaTarefaCheckListId = OSCK.ObraCronogramaTarefaCheckListId
			   LEFT JOIN Media							M		ON OSCK.MotivoImagemGuid = M.Guid
				   WHERE OSCK.EmpresaId = @EmpresaId
				     AND OSCK.ObraCronogramaTarefaChecklistId = @OrdemServicoCheckListId
					   )
			      SELECT * 
					FROM Dados ";


            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                OrdemServicoCheckListId = OrdemServicoCheckListId
            });

            return await _dapperDatabaseHelper.QueryAll<DTO.Checklist>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<int> Put(DTO.Checklist checklist, int usuarioId, int empresaId)
        {
            string script = @"
			   DECLARE @MaxId INT;

				INSERT 
				  INTO OrdemServicoChecklist
				     ( EmpresaId 
					 , ObraCronogramaTarefaLocalId
					 , ObraCronogramaTarefaCheckListId
					 , UsuarioIdAlteracao
					 , DataHoraAlteracao)
				SELECT @EmpresaId 
					 , @ObraCronogramaTarefaLocalId
					 , @ObraCronogramaTarefaCheckListId
					 , 1
					 , GETDATE()
				 WHERE @OrdemServicoCheckListId = 0
			
				   SET @MaxId = SCOPE_IDENTITY()

				INSERT 
				  INTO OrdemServicoChecklistMedia
				     ( EmpresaId
					 , OrdemServicoChecklistId
					 , MediaGuid
					 , Modelo
					 , Detalhes
					 , UsuarioId
					 , DataHora)
				SELECT @EmpresaId
					 , ISNULL(@MaxId,@OrdemServicoCheckListId)
					 , @MotivoImagemGuid
					 , CASE WHEN @Situacao = 0 THEN 'R' ELSE 'A' END
					 , CASE WHEN @Situacao = 0 
							THEN CONCAT('Imagem enviada na reprovação da inspeção do dia: ',(CONVERT(VARCHAR(10), @DataInspecao, 103) + ' '  + convert(VARCHAR(8), @DataInspecao, 14)),' pelo motivo:',@Motivo) 
							ELSE CONCAT('Imagem enviada na aprovação da inspeção no dia: ', (CONVERT(VARCHAR(10), @DataInspecao, 103) + ' '  + convert(VARCHAR(8), @DataInspecao, 14)) ) 
							 END
					 , @UsuarioId
					 , GETDATE()
				  FROM OrdemServicoChecklist		C
			 LEFT JOIN OrdemServicoChecklistMedia	M ON M.MediaGuid = @MotivoImagemGuid
				 WHERE C.OrdemServicoChecklistId = ISNULL(@MaxId,@OrdemServicoCheckListId)

                UPDATE OrdemServicoChecklist
				   SET DataInspecao = @DataInspecao
					 , Situacao = @Situacao
					 , Motivo = @Motivo
					 , MotivoImagemGuid = @MotivoImagemGuid
					 , DataHoraAlteracao = GETDATE()
				 WHERE OrdemServicoCheckListId = ISNULL(NULLIF(@OrdemServicoCheckListId,0),@MaxId)

				SELECT ISNULL(NULLIF(@OrdemServicoCheckListId,0),@MaxId)";

            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                usuarioId,
                empresaId,
                checklist.ObraCronogramaTarefaLocalId,
                checklist.ObraCronogramaTarefaCheckListId,
                checklist.DataInspecao,
                Situacao = checklist.StatusId,
                checklist.Motivo,
                checklist.MotivoImagemGuid,
                checklist.OrdemServicoCheckListId
            }); ;

            return await _dapperDatabaseHelper.ExecuteScalar<int>(script, useTransaction: true, dParams: dynamic);
        }

        public async Task<Tuple<IEnumerable<DTO.Arquivo>, int>> Arquivos(int ObraCronogramaTarefaLocalId, int ObraCronogramaTarefaCheckListId, int empresaId, DateTime? lastSync)
        {
            string script = @$"
			SELECT OS.EmpresaId
			     , OS.ObraCronogramaTarefaLocalId
				 , OS.ObraCronogramaTarefaCheckListId
			     , M.NomeArquivo					AS Nome
			     , M.Url							AS Caminho
			     , M.Extensao
			     , M.Guid
			     , OSM.DataHora						AS DataHoraAlteracao
			  FROM OrdemServicoChecklist	  OS
			  JOIN OrdemServicoChecklistMedia OSM	ON OS.OrdemServicoCheckListId = OSM.OrdemServicoChecklistId
			  JOIN Media					  M		ON OSM.MediaGuid = M.Guid
			 WHERE OS.EmpresaId = @EmpresaId
			   AND OS.ObraCronogramaTarefaLocalId = @ObraCronogramaTarefaLocalId
			   AND OS.ObraCronogramaTarefaCheckListId = @ObraCronogramaTarefaCheckListId
			   AND (@LastSync IS NULL
			    OR @LastSync > OSM.DataHora)";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                empresaId,
                ObraCronogramaTarefaLocalId,
                ObraCronogramaTarefaCheckListId,
                lastSync
            });
            //
            var result = await _dapperDatabaseHelper.QueryAll<DTO.Arquivo>(script, dParams: dynamic);

            return result;
        }
    }
}
