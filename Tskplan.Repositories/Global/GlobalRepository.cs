﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Tskplan.Data;
using Tskplan.Models.Global.DTO;
using Tskplan.Repositories.Global;
using DTO = Tskplan.Models.Global.DTO;

namespace Tskplan.Repositories.Global
{
    public class GlobalRepository : IGlobalRepository
    {
        private readonly IDapperDatabaseHelper _dapperDatabaseHelper;
        private readonly ILogger<GlobalRepository> _logger;

        public GlobalRepository(IDapperDatabaseHelper dapperDatabaseHelper, ILogger<GlobalRepository> logger)
        {
            _dapperDatabaseHelper = dapperDatabaseHelper ?? throw new ArgumentNullException(nameof(dapperDatabaseHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<ModeloServico>, int>> GetModeloServico(int empresaId, DateTime? lastSync)
        {
			string script = @$"
		            SELECT S.ServicoId		AS ModeloServicoId
						 , S.Nome
						 , S.GrupoCompraId
						 , S.Guid
					  FROM Servico S
					 WHERE S.EmpresaId = @EmpresaId
					   AND (@LastSync IS NULL
					    OR  S.DataHoraAlteracao > @LastSync)";
			//
			DynamicParameters dynamic = new DynamicParameters();
			dynamic.AddDynamicParams(new
			{
				EmpresaId = empresaId,
				LastSync = lastSync
			});
			//
			return await _dapperDatabaseHelper.QueryAll<DTO.ModeloServico>(script, dParams: dynamic, useTransaction: true);
		}

        public async Task<Tuple<IEnumerable<DTO.Obra>, int>> GetObra(int empresaId, DateTime? lastSync)
        {
			string script = @$"
		            SELECT O.ObraId
						 , O.Nome
						 , L.LocalEntrega		AS Endereco
						 , O.Guid
					  FROM Obra				O
			     LEFT JOIN V_LocalEntrega	L	ON O.LocalEntregaId = L.LocalEntregaId
					 WHERE O.EmpresaId = @EmpresaID
				       AND (@LastSync IS NULL
				        OR  O.DataHoraAlteracao > @LastSync)";
			//
			DynamicParameters dynamic = new DynamicParameters();
			dynamic.AddDynamicParams(new
			{
				EmpresaId = empresaId,
				LastSync = lastSync
			});
			//
			return await _dapperDatabaseHelper.QueryAll<DTO.Obra>(script, dParams: dynamic, useTransaction: true);
		}

        public async Task<Tuple<IEnumerable<UnidadeMedida>, int>> GetUnidadeMedida(int empresaId, DateTime? lastSync)
        {
			string script = @$"
		            SELECT U.UnidadeMedidaId
						 , U.Nome
						 , U.Guid
					  FROM UnidadeMedida U
					 WHERE U.EmpresaId = @EmpresaID
				       AND (@LastSync IS NULL
				        OR  U.DataHoraAlteracao > @LastSync)";
			//
			DynamicParameters dynamic = new DynamicParameters();
			dynamic.AddDynamicParams(new
			{
				EmpresaId = empresaId,
				LastSync = lastSync
			});
			//
			return await _dapperDatabaseHelper.QueryAll<DTO.UnidadeMedida>(script, dParams: dynamic, useTransaction: true);
		}
    }
}
