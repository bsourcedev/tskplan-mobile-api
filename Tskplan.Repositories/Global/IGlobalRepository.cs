﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Global.DTO;

namespace Tskplan.Repositories.Global
{
    public interface IGlobalRepository
    {
        Task<Tuple<IEnumerable<DTO.Obra>, int>> GetObra(int empresaId, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.UnidadeMedida>, int>> GetUnidadeMedida(int empresaId, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.ModeloServico>, int>> GetModeloServico(int empresaId, DateTime? lastSync);

    }
}
