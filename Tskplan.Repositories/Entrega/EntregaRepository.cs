﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tskplan.Data;
using DTO = Tskplan.Models.Entrega.DTO;

namespace Tskplan.Repositories.Entrega
{
    public class EntregaRepository : IEntregaRepository
    {
        private readonly IDapperDatabaseHelper _dapperDatabaseHelper;
        private readonly ILogger<EntregaRepository> _logger;

        public EntregaRepository(IDapperDatabaseHelper dapperDatabaseHelper, ILogger<EntregaRepository> logger)
        {
            _dapperDatabaseHelper = dapperDatabaseHelper ?? throw new ArgumentNullException(nameof(dapperDatabaseHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #region Entrega

        public async Task<Tuple<IEnumerable<DTO.Entrega>, int>> Get(int empresaId, string data, bool processados, DateTime? lastSync)
        {
            string script = @"
                SELECT E.EmpresaId
            		 , E.EntregaId
            		 , E.PedidoCompraId
            		 , E.DataEntrega						AS Data
            		 , E.Observacao
            		 , E.NotaFiscal
            		 , E.Serie
            		 , F.FornecedorId
            		 , ISNULL(F.NomeFantasia,F.RazaoSocial) AS Fornecedor
            		 , O.ObraId
            		 , O.Nome								AS Obra
					 , CASE WHEN EP.EntregaId IS NULL
							THEN CAST(0 AS BIT)
							ELSE CAST(1 AS BIT)
					   END									AS Processado
					 , E.Guid
            	  FROM Entrega					E
			 LEFT JOIN EntregaEstoqueProcessado	EP	ON E.EntregaId = EP.EntregaId
             LEFT JOIN PedidoCompra				P	ON E.PedidoCompraId = P.PedidoCompraId
             LEFT JOIN Fornecedor				F	ON P.FornecedorId = F.FornecedorId
             LEFT JOIN V_PedidoCompra_Obra		PO	ON P.PedidoCompraId = PO.PedidoCompraId
             LEFT JOIN Obra						O   ON PO.ObraId = O.ObraId
                 WHERE E.EmpresaId = @EmpresaId
                   AND (@Processados = CAST(0 AS BIT) AND EP.EntregaId IS NULL
                    OR  @Processados = CAST(1 AS BIT))
                   AND (@Data = ''
                    OR CONVERT(DATETIME, @Data, 103) = CAST(E.DataEntrega AS date))
                   AND (@LastSync IS NULL 
                    OR GETDATE() > @LastSync)
              ORDER BY EntregaId
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                Data = data,
                Processados = processados,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.Entrega>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<DTO.Entrega> Put(DTO.Entrega entrega, int empresaId, int usuarioId)
        {
            string script = @"
                UPDATE Entrega
			       SET PedidoCompraId       = NULLIF(@PedidoCompraId,0)
			   	     , DataEntrega          = @DataEntrega
			   	     , Observacao           = @Observacao
			   	     , NotaFiscal           = @NotaFiscal
			   	     , Serie                = @Serie
			   	     , UsuarioIdAlteracao   = @UsuarioId
			   	     , DataHoraAlteracao    = GETDATE()
				 WHERE EntregaId = @EntregaId
			    
			    SELECT E.EmpresaId
            		 , E.EntregaId
            		 , E.PedidoCompraId
            		 , E.DataEntrega            AS Data
            		 , E.Observacao
            		 , E.NotaFiscal
            		 , E.Serie
            		 , F.FornecedorId
            		 , ISNULL(F.NomeFantasia,F.RazaoSocial) AS Fornecedor
            		 , O.ObraId
            		 , O.Nome					AS Obra
            	  FROM Entrega				E
             LEFT JOIN PedidoCompra			P	ON E.PedidoCompraId = P.PedidoCompraId
             LEFT JOIN Fornecedor			F	ON P.FornecedorId = F.FornecedorId
             LEFT JOIN V_PedidoCompra_Obra	PO	ON P.PedidoCompraId = PO.PedidoCompraId
             LEFT JOIN Obra					O   ON PO.ObraId = O.ObraId
                 WHERE E.EntregaId = @EntregaId";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                entrega.PedidoCompraId,
                DataEntrega = entrega.Data,
                entrega.Observacao,
                entrega.NotaFiscal,
                entrega.Serie,
                UsuarioId = usuarioId,
                entrega.EntregaId
            });
            //
            var result = await _dapperDatabaseHelper.QueryOne<DTO.Entrega>(script, dParams: dynamic, useTransaction: true);

            return result;
        }

        public async Task<DTO.Entrega> Post(DTO.Entrega entrega, int empresaId, int usuarioId)
        {
            string script = @"
          	   DECLARE @MaxId INT;

			    INSERT  
			      INTO Entrega
			         ( EmpresaId
			   	     , PedidoCompraId
			   	     , DataEntrega
			   	     , Observacao
			   	     , NotaFiscal
			   	     , Serie
			   	     , UsuarioIdInclusao
			   	     , DataHoraInclusao
			   	     , UsuarioIdAlteracao
			   	     , DataHoraAlteracao)
			    SELECT @EmpresaId
			   	     , NULLIF(@PedidoCompraId,0)
			   	     , @DataEntrega
			   	     , @Observacao
			   	     , @NotaFiscal
			   	     , @Serie
			   	     , @usuarioId
			   	     , GETDATE()
			   	     , @UsuarioId
			   	     , GETDATE()
			   
			    SELECT @MaxId = SCOPE_IDENTITY();

			    SELECT E.EmpresaId
            		 , E.EntregaId
            		 , E.PedidoCompraId
            		 , E.DataEntrega            AS Data
            		 , E.Observacao
            		 , E.NotaFiscal
            		 , E.Serie
            		 , F.FornecedorId
            		 , ISNULL(F.NomeFantasia,F.RazaoSocial) AS Fornecedor
            		 , O.ObraId
            		 , O.Nome					AS Obra
            	  FROM Entrega				E
             LEFT JOIN PedidoCompra			P	ON E.PedidoCompraId = P.PedidoCompraId
             LEFT JOIN Fornecedor			F	ON P.FornecedorId = F.FornecedorId
             LEFT JOIN V_PedidoCompra_Obra	PO	ON P.PedidoCompraId = PO.PedidoCompraId
             LEFT JOIN Obra					O   ON PO.ObraId = O.ObraId
                 WHERE E.EntregaId = @MaxId
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                entrega.PedidoCompraId,
                DataEntrega = entrega.Data,
                entrega.Observacao,
                entrega.NotaFiscal,
                entrega.Serie,
                UsuarioId = usuarioId
            });
            //
            var result = await _dapperDatabaseHelper.QueryOne<DTO.Entrega>(script, dParams: dynamic, useTransaction: true);

            return result;
        }

        #endregion

        #region Entrega Produto

        public async Task<Tuple<IEnumerable<DTO.Produtos>, int>> GetProdutos(int entregaId, int empresaId, DateTime ? lastSync)
        {
            string script = @"
                  WITH R1 AS (
        	    SELECT EP.EntregaProdutoId
        			 , EP.EntregaId
        			 , CASE 
        				WHEN EP.PedidoCompraItemId IS NULL
        				THEN CAST(0 AS BIT)
        				ELSE CAST(1 AS BIT)
        			   END						AS Pedido
        			 , EP.ProdutoId
        			 , P.Nome					AS Produto
        			 , EP.Quantidade
        			 , EP.Observacao
					 , EP.Guid
        		  FROM EntregaProduto		EP
        		  JOIN Produto				P	ON EP.ProdutoId = P.ProdutoId
        	 	  JOIN Usuario				UI	ON EP.UsuarioIdInclusao = UI.UsuarioId
        	 	  JOIN Usuario				UA	ON EP.UsuarioIdAlteracao = UA.UsuarioId
        	 LEFT JOIN PedidoCompraItem		PCI	ON EP.PedidoCompraItemId = PCI.PedidoCompraItemId
        	 LEFT JOIN CotacaoFinalizacao	CF	ON PCI.CotacaoFinalizacaoId = CF.CotacaoFinalizacaoId	 
        		 WHERE EP.EntregaId = @EntregaId
        		   AND EP.EmpresaId = @EmpresaId
                   AND (@LastSync IS NULL 
                    OR EP.DataHoraAlteracao > @LastSync)   
        			 )
        	    SELECT R.*             
        			 , COUNT(1) OVER() AS TotalLinhas
                 FROM R1            R           
             ORDER BY EntregaProdutoId
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                EntregaId = entregaId,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.Produtos>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<DTO.Produtos> PutProdutos(DTO.Produtos produto, int empresaId, int usuarioId)
        {
            string script = @"
                UPDATE EntregaProduto
		           SET Quantidade = @Quantidade
		        	 , Observacao = @Observacao
		        	 , UsuarioIdAlteracao = @UsuarioId
		        	 , DataHoraAlteracao = GETDATE()
		         WHERE EntregaProdutoId = @EntregaProdutoId;

                  WITH R1 AS (
        	    SELECT EP.EntregaProdutoId
        			 , EP.EntregaId
        			 , CASE 
        				WHEN EP.PedidoCompraItemId IS NULL
        				THEN CAST(0 AS BIT)
        				ELSE CAST(1 AS BIT)
        			   END						AS Pedido
        			 , EP.ProdutoId
        			 , P.Nome					AS Produto
        			 , EP.Quantidade
        			 , EP.Observacao
        		  FROM EntregaProduto		EP
        		  JOIN Produto				P	ON EP.ProdutoId = P.ProdutoId
        	 	  JOIN Usuario				UI	ON EP.UsuarioIdInclusao = UI.UsuarioId
        	 	  JOIN Usuario				UA	ON EP.UsuarioIdAlteracao = UA.UsuarioId
        	 LEFT JOIN PedidoCompraItem		PCI	ON EP.PedidoCompraItemId = PCI.PedidoCompraItemId
        	 LEFT JOIN CotacaoFinalizacao	CF	ON PCI.CotacaoFinalizacaoId = CF.CotacaoFinalizacaoId	 
        		 WHERE EP.EntregaProdutoId = @EntregaProdutoId
        			 )
        	    SELECT R.*             
        			 , COUNT(1) OVER() AS TotalLinhas
                 FROM R1            R      
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                produto.Quantidade,
                produto.Observacao,
                UsuarioId = usuarioId,
                produto.EntregaProdutoId
            });
            //
            var result = await _dapperDatabaseHelper.QueryOne<DTO.Produtos>(script, dParams: dynamic, useTransaction: true);

            return result;
        }

        public async Task<DTO.Produtos> PostProdutos(DTO.Produtos produto, int empresaId, int usuarioId)
        {
            string script = @"
               DECLARE @MaxId INT;

        		INSERT 
        		  INTO EntregaProduto
        		     ( EmpresaId
        			 , EntregaId
        			 , ProdutoId
        			 , Quantidade
        			 , Observacao
        			 , UsuarioIdInclusao
        			 , DataHoraInclusao
        			 , UsuarioIdAlteracao
        			 , DataHoraAlteracao)
        		SELECT @EmpresaId
        			 , @EntregaId
        			 , @ProdutoId
        			 , @Quantidade
        			 , @Observacao
        			 , @UsuarioId
        			 , GETDATE()
        			 , @UsuarioId
        			 , GETDATE()
        			 
                   SET @MaxId = SCOPE_IDENTITY();  
        
        		  WITH R1 AS (
                SELECT EP.EntregaProdutoId
            		 , EP.EntregaId
            		 , CASE 
            			WHEN EP.PedidoCompraItemId IS NULL
            			THEN CAST(0 AS BIT)
            			ELSE CAST(1 AS BIT)
            		   END						AS Pedido
            		 , EP.ProdutoId
            		 , P.Nome					AS Produto
            		 , EP.Quantidade
            		 , EP.Observacao
            	  FROM EntregaProduto		EP
            	  JOIN Produto				P	ON EP.ProdutoId = P.ProdutoId
             	  JOIN Usuario				UI	ON EP.UsuarioIdInclusao = UI.UsuarioId
             	  JOIN Usuario				UA	ON EP.UsuarioIdAlteracao = UA.UsuarioId
             LEFT JOIN PedidoCompraItem		PCI	ON EP.PedidoCompraItemId = PCI.PedidoCompraItemId
             LEFT JOIN CotacaoFinalizacao	CF	ON PCI.CotacaoFinalizacaoId = CF.CotacaoFinalizacaoId	 
            	 WHERE EP.EmpresaId = @EmpresaId
        		   AND EP.EntregaProdutoId = @MaxId
            		 )
                SELECT R.*             
            		 , COUNT(1) OVER() AS TotalLinhas
                  FROM R1            R  
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                produto.EntregaId,
                produto.ProdutoId,
                produto.Quantidade,
                produto.Observacao,
                UsuarioId = usuarioId
            });
            //
            var result = await _dapperDatabaseHelper.QueryOne<DTO.Produtos>(script, dParams: dynamic, useTransaction: true);

            return result;
        }

        #endregion

        #region Outros

        public async Task<Tuple<IEnumerable<DTO.ProdutosLista>, int>> GetProdutosLista(int empresaId, int fornecedorId, string query, DateTime? lastSync)
        {
            query = query != "" ? $"%{query}%" : "";

            string script = @"
        		  WITH Grupos AS (
        		SELECT DISTINCT FGC.GrupoCompraId
        		  FROM FornecedorGrupoCompra FGC
        		 WHERE NULLIF(@FornecedorId,0) IS NULL 
        		    OR FGC.FornecedorId = NULLIF(@FornecedorId,0)
        		     )
        		SELECT P.ProdutoId
        			 , P.Nome	 AS Produto
					 , P.Guid
        		  FROM Produto P
        	 LEFT JOIN Grupos  G ON P.GrupoCompraId = G.GrupoCompraId
        		 WHERE P.EmpresaId = @EmpresaId
        		   AND (NULLIF(@FornecedorId,0) IS NULL
        		    OR NULLIF(@FornecedorId,0)  IS NOT NULL 
        		   AND G.GrupoCompraId IS NOT NULL)
                   AND (@Query = '' 
                    OR P.Nome LIKE @Query COLLATE SQL_Latin1_General_CP1_CI_AI)
                   AND (@LastSync IS NULL 
                    OR P.DataHoraAlteracao > @LastSync)   
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                FornecedorId = fornecedorId,
                Query = query,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.ProdutosLista>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.Pedido>, int>> GetPedidos(int empresaId, int fornecedorId, DateTime? lastSync)
        {
            string script = @"
                  WITH EntregueComPedido AS (
        		SELECT PCNFEI.PedidoCompraId
        			 , SUM(PCNFEI.Quantidade)						AS Quantidade
        			 , SUM(NFEI.ValorTotal)							AS Valor
        		  FROM PedidoCompraNotaFiscalEntradaItem	PCNFEI
        		  JOIN NotaFiscalEntradaItem				NFEI	ON NFEI.NotaFiscalEntradaItemId = PCNFEI.NotaFiscalEntradaItemId
        	  GROUP BY PCNFEI.PedidoCompraId
        			 )
        			 , EntregueSemPedido AS (
        		SELECT E.PedidoCompraId
        			 , SUM(EP.Quantidade)									AS Quantidade
        			 , SUM(ISNULL(EP.Quantidade,0) * ISNULL(CF.Valor,0))	AS Valor
        		  FROM EntregaProduto			EP
        		  JOIN Entrega					E	ON EP.EntregaId = E.EntregaId
        		  JOIN EntregaEstoqueProcessado	EEP ON E.EntregaId = EEP.EntregaId
        		  JOIN PedidoCompraItem			PCI	ON PCI.PedidoCompraId = E.PedidoCompraId
        		  JOIN CotacaoFinalizacao		CF	ON CF.CotacaoFinalizacaoId = PCI.CotacaoFinalizacaoId
        										   AND CF.ProdutoId = EP.ProdutoId
        	  GROUP BY E.PedidoCompraId
        			 )
        			 , EntregueSoma AS (
        		SELECT *
        		  FROM EntregueComPedido	ECP
        		 UNION ALL
        		SELECT * 
        		  FROM EntregueSemPedido				 	
        			 )
        			 , Entregue AS (			 
        		SELECT ES.PedidoCompraId
        			 , SUM(ES.Quantidade)					AS Quantidade
        			 , SUM(ES.Valor)						AS Valor
        		  FROM EntregueSoma ES
        	  GROUP BY ES.PedidoCompraId
        			 )
        			 , Quantidades AS (
        		SELECT SUM(CF.QuantidadeFechada)			AS Quantidade
        			 , PC.PedidoCompraId
        		  FROM PedidoCompraItem				T
        		  JOIN PedidoCompra					PC		ON PC.PedidoCompraId = T.PedidoCompraId
        		  JOIN CotacaoFinalizacao			CF		ON CF.CotacaoFinalizacaoId = T.CotacaoFinalizacaoId
        	  GROUP BY PC.PedidoCompraId
        			 )
        		     , R1 AS (        
        	    SELECT T.PedidoCompraId
        			 , F.FornecedorId
        			 , F.RazaoSocial										AS Fornecedor			 
        			 , T.DataHoraInclusao									AS Data
        			 , (ISNULL(E.Quantidade,0)/ISNULL(Q.Quantidade,1))*100  AS Porcentagem
        			 , O.ObraId
        			 , O.Nome												AS Obra
        		     , LC.LocalEntrega										AS ObraEndereco
        			 , PCS.PedidoCompraStatusId								AS StatusId
        			 , PCS.Nome												AS Status
                     , T.Guid
        		  FROM PedidoCompra						T
        		  JOIN PedidoCompraStatus				PCS ON PCS.PedidoCompraStatusId = T.PedidoCompraStatusId		 
        	 LEFT JOIN V_PedidoCompra_Obra				PO	ON T.PedidoCompraId = PO.PedidoCompraId
        	 LEFT JOIN Obra								O	ON O.ObraId = PO.ObraId
        	 LEFT JOIN V_LocalEntrega					LC	ON O.LocalEntregaId = LC.LocalEntregaId
        	 LEFT JOIN Fornecedor						F	ON F.FornecedorId = T.FornecedorId
        	 LEFT JOIN Quantidades						Q   ON T.PedidoCompraId = Q.PedidoCompraId
        	 LEFT JOIN Entregue							E   ON T.PedidoCompraId = E.PedidoCompraId 
        		
        		 WHERE T.EmpresaId = @EmpresaId
                   AND (@FornecedorId = 0 OR T.FornecedorId = @FornecedorId)
        		   AND T.PedidoCompraStatusId IN (2,5) -- 2 = Aprovado 5 = Entrega Parcial
                   AND (@LastSync IS NULL 
                    OR T.DataHoraAlteracao > @LastSync)
        			 )
        	    SELECT DISTINCT R.*               
        		     , COUNT(1) OVER() AS TotalLinhas               
                  FROM R1 R              
              ORDER BY PedidoCompraId
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                FornecedorId = fornecedorId,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.Pedido>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.Fornecedor>, int>> GetFornecedores(int empresaId, DateTime? lastSync)
        {
            string script = @"
                WITH R1 AS (        
        	    SELECT DISTINCT T.FornecedorId
					 , ISNULL(F.NomeFantasia,F.RazaoSocial) AS Nome
					 , T.Guid
        		  FROM PedidoCompra	T
				  JOIN Fornecedor	F ON T.FornecedorId = F.FornecedorId
        		 WHERE T.EmpresaId = @EmpresaId
        		   AND T.PedidoCompraStatusId IN (2,5) -- 2 = Aprovado 5 = Entrega Parcial
                   AND (@LastSync IS NULL 
                    OR T.DataHoraAlteracao > @LastSync)
        			 )
        	    SELECT R.*               
        		     , COUNT(1) OVER() AS TotalLinhas               
                  FROM R1 R              
              ORDER BY Nome
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.Fornecedor>(script, dParams: dynamic, useTransaction: true);
        }

        #endregion

    }
}
