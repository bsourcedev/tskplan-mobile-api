﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Tskplan.Data;
using Tskplan.Models.Chat.DTO;
using DTO = Tskplan.Models.Chat.DTO;

namespace Tskplan.Repositories.Chat
{
    public class ChatRepository : IChatRepository
    {
        private readonly IDapperDatabaseHelper _dapperDatabaseHelper;
        private readonly ILogger<ChatRepository> _logger;

        public ChatRepository(IDapperDatabaseHelper dapperDatabaseHelper, ILogger<ChatRepository> logger)
        {
            _dapperDatabaseHelper = dapperDatabaseHelper ?? throw new ArgumentNullException(nameof(dapperDatabaseHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<DTO.Chat>, int>> Get(int usuarioId, int empresaId, DateTime? lastSync)
        {
            string script = @$"
		        SELECT C.EmpresaId
		        	 , C.TarefaChatId
		        	 , C.TarefaId
		        	 , C.Guid
		        	 , C.DataHoraInclusao
		        	 , T.Nome					AS Titulo
		        	 , O.Nome					AS Obra
		        	 , CAST(1 AS BIT)			AS Grupo
		        	 , 'Mensagem'				AS UltimaMensagem
		        	 , 'Usuario'				AS UltimaMensagemUsuario
		        	 , GETDATE()				AS UltimaMensagemData
		          FROM TarefaChat			C
		          JOIN ObraCronogramaTarefa	T	ON C.TarefaId = T.ObraCronogramaTarefaId
		          JOIN Obra					O	ON T.ObraId = O.ObraId
		         WHERE T.EmpresaId = @EmpresaId";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                empresaId,
                usuarioId,
                lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.Chat>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.Servers>, int>> GetServers(bool todosServers, int empresaId)
        {
            string script = @$"
		       SELECT O.ObraId  
                    , O.Guid												AS Id  
                    , O.Nome												AS ServerName  
                    , ISNULL(NULLIF(O.Sigla,''), [dbo].fnFirsties(O.Nome))	AS ShortName  
                    , ISNULL(A.Caminho,'')									AS IconUrl  
					, O.Guid                                                AS guid
                 FROM Obra		O  
            LEFT JOIN Arquivo	A ON O.FotoArquivoId = A.ArquivoId
                WHERE O.EmpresaId = @EmpresaId  
                  AND (@TodosServers = CAST(1 AS BIT) OR (O.StatusId = 1 AND @TodosServers = CAST(0 AS BIT)))
                ORDER BY ObraId   ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                todosServers,
                empresaId,
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.Servers>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task PostMessage(MessageData message)
        {

            string script = @$"
             DECLARE @UserId int = (SELECT UsuarioId from [Gestaoa_producao].[dbo].usuario where [login] = @user);

		        EXEC [Mensageria].[dbo].S_RegisterMesssage_E 
                     @UserId
                   , @messageId
                   , @messageType
                   , @messageText
                   , @senderCreationDate
                   , @urlItem
                   , @channel
                   , @responseGuid";

            //
#if DEBUG
            script = @$"
             DECLARE @UserId int = (SELECT UsuarioId from [Tskplan].[dbo].usuario where [login] = @user);

		        EXEC [Mensageria].[dbo].S_RegisterMesssage_E 
                     @UserId
                   , @messageId
                   , @messageType
                   , @messageText
                   , @senderCreationDate
                   , @urlItem
                   , @channel
                   , @responseGuid";
#endif
            //
            DynamicParameters dynamic = new DynamicParameters();

            dynamic.Add("user", message.user);
            dynamic.Add("messageId", message.messageId, DbType.Guid);
            dynamic.Add("messageType", message.messageType);
            dynamic.Add("messageText", message.messageText);
            dynamic.Add("senderCreationDate", message.senderCreationDate.ToUniversalTime());
            dynamic.Add("urlItem", message.urlItem);
            dynamic.Add("channel", message.channel, DbType.Guid);
            dynamic.Add("responseGuid", message.responseGuid, DbType.Guid);

            await _dapperDatabaseHelper.Execute(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<MessageData>, int>> ObtemMensagensDoCanal(Guid channel)
        {
            string script = @$"EXEC [Mensageria].[dbo].S_MSG_LOADCHANNELMESSAGES @ChannelGuid";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                ChannelGuid = channel
            });
            var result = await _dapperDatabaseHelper.QueryAll<DTO.MessageData>(script, dParams: dynamic, useTransaction: true);

            foreach (MessageData message in result.Item1)
                message.senderCreationDate = DateTime.SpecifyKind(message.senderCreationDate, DateTimeKind.Utc);
            //
            return result;
        }

        public async Task<Tuple<IEnumerable<ChannelData>, int>> ObtemCanais(int obraId, string pesquisa)
        {
            string script = @$"
            DECLARE @Query VARCHAR(MAX) = CASE WHEN ISNULL(@Pesquisa, '') = '' THEN '' ELSE '%' + @Pesquisa + '%' END; 
      
			 SELECT DISTINCT T.Nome							                AS Tarefa
				  , T.ObraCronogramaTarefaId				                AS TarefaId
				  , T.Guid
				  , O.ObraId
				  , O.Nome									                AS Obra
				  , O.Guid									                AS Server
				  , O.Nome													AS ServerName
                  , ISNULL(NULLIF(O.Sigla,''), [dbo].fnFirsties(O.Nome))	AS ServerShortName  
				  , T.Nome													AS ChannelName
				  , T.Guid													AS Channel
			   FROM ObraCronogramaTarefa				T
			   JOIN Obra								O	ON T.ObraId = O.ObraId
	      LEFT JOIN [Mensageria].[dbo].[Channel]		C	ON C.ChannelGuid = T.Guid
		  LEFT JOIN [Mensageria].[dbo].[ChannelMember]	M	ON M.ChannelId = C.ChannelId
														   --AND M.UserId = @UsuarioId
			  WHERE M.ChannelMemberId IS NULL	
			    AND (O.ObraId = @ObraId 
				 OR  @ObraId = 0)
				AND (@Query = ''
			     OR T.Nome					LIKE @Query COLLATE SQL_Latin1_General_CP1_CI_AI)";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                pesquisa,
                obraId
            });
            //
            var retorno = await _dapperDatabaseHelper.QueryAll<DTO.ChannelData>(script, dParams: dynamic, useTransaction: true);
            //
            return retorno;
        }

        public async Task<Tuple<IEnumerable<ChannelData>, int>> ObtemCanaisDoUsuario(string usuario)
        {
            string script = @$"EXEC [Mensageria].[dbo].S_MSG_LoadUserChannels @Usuario";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                usuario
            });
            //
            var retorno = await _dapperDatabaseHelper.QueryAll<DTO.ChannelData>(script, dParams: dynamic, useTransaction: true);
            return retorno;
        }

        public async Task RegistrarToken(string token, int usuarioId)
        {
            string script = @$"
		        EXEC [Mensageria].[dbo].S_MSG_RT 
                     @userId
                   , @token";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                token,
                usuarioId
            });

            await _dapperDatabaseHelper.Execute(script, dParams: dynamic, useTransaction: true);
        }

        public async Task RegistrarUsuarioAoCanal(Guid channel, int usuarioId)
        {
            string script = @$"
		        EXEC [Mensageria].[dbo].S_RegisterUserChannel_E 
                     @userId
                   , @channel";
            //
            DynamicParameters dynamic = new DynamicParameters();

            dynamic.Add("userId", usuarioId);
            dynamic.Add("channel", channel, DbType.Guid);

            await _dapperDatabaseHelper.Execute(script, dParams: dynamic, useTransaction: true);
        }

    }
}
