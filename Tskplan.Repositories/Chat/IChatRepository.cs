﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tskplan.Models.Chat.DTO;
using DTO = Tskplan.Models.Chat.DTO;

namespace Tskplan.Repositories.Chat
{
    public interface IChatRepository
    {
        Task<Tuple<IEnumerable<DTO.Chat>, int>> Get(int usuarioId, int empresaId, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.Servers>, int>> GetServers(bool todosServers, int empresaId);
        Task PostMessage(MessageData message);
        Task RegistrarToken(string token, int usuarioId);
        Task RegistrarUsuarioAoCanal(Guid channel, int usuarioId);
        Task<Tuple<IEnumerable<MessageData>, int>> ObtemMensagensDoCanal(Guid channel);
        Task<Tuple<IEnumerable<ChannelData>, int>> ObtemCanaisDoUsuario(string usuario);
        Task<Tuple<IEnumerable<ChannelData>, int>> ObtemCanais(int obraId, string pesquisa);
    }
}
