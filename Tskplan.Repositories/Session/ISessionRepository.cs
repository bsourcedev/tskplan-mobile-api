﻿using System.Threading.Tasks;
using DTO = Tskplan.Models.Session.DTO;
using Tskplan.Models.Session.ViewModel;

namespace Tskplan.Repositories.Session
{
    public interface ISessionRepository
    {
        Task<DTO.Session> Create(SessionPost item);
    }
}
