﻿using System;
using System.Threading.Tasks;
using System.Text;
using Tskplan.Data;
using Tskplan.Models.Configuration;
using Microsoft.Extensions.Logging;
using Tskplan.Models.Session.ViewModel;
using DTO = Tskplan.Models.Session.DTO;
using Dapper;

namespace Tskplan.Repositories.Session
{
    public class SessionRepository: ISessionRepository
    {
        private readonly IDapperDatabaseHelper _dapperDatabaseHelper;
        private readonly ILogger<SessionRepository> _logger;

        public SessionRepository(IDapperDatabaseHelper dapperDatabaseHelper, ILogger<SessionRepository> logger)
        {
            _dapperDatabaseHelper = dapperDatabaseHelper ?? throw new ArgumentNullException(nameof(dapperDatabaseHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<DTO.Session> Create(SessionPost item)
        {
            string script = @"
                SELECT EmpresaId
                     , UsuarioId AS Id
                     , Nome
                     , Senha
                     , Email
                  FROM Usuario U
                 WHERE Email = @Login
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                Login = item.User
            });
            //
            return await _dapperDatabaseHelper.QueryOne<DTO.Session>(script, dParams: dynamic, useTransaction: true);
        }
    }
}
