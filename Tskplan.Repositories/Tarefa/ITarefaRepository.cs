﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Tarefa.DTO;

namespace Tskplan.Repositories.Tarefa
{
    public interface ITarefaRepository
    {
        Task<Tuple<IEnumerable<DTO.TarefaListaLocais>, int>> GetListaLocais(int obraId, int obraLocalId, int empresaId, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.OS>, int>> GetOS(Guid tarefaGuid, int empresaId, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.OSLocais>, int>> GetOSLocais(int obraId, int tarefaId, int empresaId, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.OSLocais>, int>> GetOSLocaisAuxiliar(int empresaId, DateTime? lastSync);
        Task PostOS(DTO.OS os, int empresaId, int usuarioId);
        Task PutOS(DTO.OS os, int empresaId, int usuarioId);

        Task<Tuple<IEnumerable<DTO.TarefaLista>, int>> GetLista(int obraId, int empresaId, DateTime? lastSync, int? statusId, string? query);
        Task<Tuple<IEnumerable<DTO.Tarefa>, int>> Get(Guid tarefaGuid, int empresaId);
        Task Post(DTO.Tarefa tarefa, int empresaId, int usuarioId);
        Task Put(DTO.Tarefa tarefa, int empresaId, int usuarioId);

        Task<Tuple<IEnumerable<DTO.Arquivo>, int>> Arquivos(Guid tarefaGuid, int empresaId, DateTime? lastSync);
    }
}
