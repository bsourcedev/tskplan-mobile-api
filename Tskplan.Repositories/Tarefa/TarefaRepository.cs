﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Tskplan.Data;
using DTO = Tskplan.Models.Tarefa.DTO;

namespace Tskplan.Repositories.Tarefa
{
    public class TarefaRepository : ITarefaRepository
    {
        private readonly IDapperDatabaseHelper _dapperDatabaseHelper;
        private readonly ILogger<TarefaRepository> _logger;

        public TarefaRepository(IDapperDatabaseHelper dapperDatabaseHelper, ILogger<TarefaRepository> logger)
        {
            _dapperDatabaseHelper = dapperDatabaseHelper ?? throw new ArgumentNullException(nameof(dapperDatabaseHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #region OS

        public async Task<Tuple<IEnumerable<DTO.TarefaListaLocais>, int>> GetListaLocais(int obraId, int obraLocalId, int empresaId, DateTime? lastSync)
        {
            string script = @$"
		            WITH TarefasChecklist AS (
   			  	  SELECT C.ObraCronogramaTarefaId
   			  		   , COUNT (1) AS Total
   			  	    FROM V_OrdemServico_Checklist	 C
				    JOIN V_Tarefa_Andamento			 A ON C.ObraCronogramaTarefaId = A.ObraCronogramaTarefaId
				   WHERE (C.ObraLocalId = @ObraLocalId 
                      OR  @ObraLocalId IS NULL)
                     AND C.EmpresaId = @EmpresaId
					 AND C.ObraId = @ObraId
					 AND A.Porcentagem > 0
   			    GROUP BY C.ObraCronogramaTarefaId
   			  		   )
	              SELECT T.ObraCronogramaTarefaId
		               , OS.ObraCronogramaTarefaLocalId
		               , T.Nome							AS Tarefa
					   , TC.Total						AS QuantidadeChecklist
					   , T.ObraId
					   , OS.ObraLocalId
					   , OS.Guid
	                FROM ObraCronogramaTarefaLocal	OS
		            JOIN ObraCronogramaTarefa		T	ON OS.ObraCronogramaTarefaId = T.ObraCronogramaTarefaId
					JOIN TarefasChecklist			TC	ON T.ObraCronogramaTarefaId = TC.ObraCronogramaTarefaId
	               WHERE OS.ObraLocalId = @ObraLocalId
	                 AND T.ObraId		= @ObraId
                     AND (@LastSync IS NULL 
                      OR  OS.DataHoraAlteracao > @LastSync)
				ORDER BY T.Nome";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                ObraId = obraId,
                ObraLocalId = obraLocalId,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.TarefaListaLocais>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.OSLocais>, int>> GetOSLocais(int obraId, int tarefaId, int empresaId, DateTime? lastSync)
        {
            string script = @"
				 ;WITH Locais AS (
	        	SELECT L.ObraLocalId 
	        		 , L.ObraId
	                 , L.Caminho
	                 , CASE 
	                  		WHEN LEN(replace(RIGHT(Caminho,charindex('\',reverse(Caminho),1)+0),'\',''))>0
	                  			THEN replace(RIGHT(Caminho,charindex('\',reverse(Caminho),1)+0),'\','')
	                  	    ELSE Caminho
	                   END				  AS Nome
	        		 , L.Guid
	        	  FROM ObraLocal					L
	         LEFT JOIN ObraCronogramaTarefaLocal	OS   ON OS.ObraLocalId = L.ObraLocalId
	        											AND OS.ObraCronogramaTarefaId = @TarefaId
	        	 WHERE L.ObraId = @ObraId
	        	   AND OS.ObraCronogramaTarefaLocalId IS NULL
	        	     )
	            SELECT L.* 
	                 , P.ObraLocalId	 AS PaiId
                     , @TarefaId         AS TarefaId
	        	  FROM Locais		L
	         LEFT JOIN ObraLocal	P    ON P.Caminho = REPLACE(L.Caminho ,CONCAT('\',L.Nome),'')
	        							AND L.ObraId = P.ObraId
	        							AND L.ObraLocalId != P.ObraLocalId
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                ObraId = obraId,
                EmpresaId = empresaId,
                TarefaId = tarefaId,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.OSLocais>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.OSLocais>, int>> GetOSLocaisAuxiliar(int empresaId, DateTime? lastSync)
        {
            string script = @"
				 ;WITH Locais AS (
	        	SELECT L.ObraLocalId 
	        		 , L.ObraId
	                 , L.Caminho
	                 , CASE 
	                  		WHEN LEN(replace(RIGHT(Caminho,charindex('\',reverse(Caminho),1)+0),'\',''))>0
	                  			THEN replace(RIGHT(Caminho,charindex('\',reverse(Caminho),1)+0),'\','')
	                  	    ELSE Caminho
	                   END				  AS Nome
	        		 , L.Guid
	        	  FROM ObraLocal L
	        	     )
	            SELECT L.* 
	                 , P.ObraLocalId	 AS PaiId
	        	  FROM Locais		L
	         LEFT JOIN ObraLocal	P    ON P.Caminho = REPLACE(L.Caminho ,CONCAT('\',L.Nome),'')
	        							AND L.ObraId = P.ObraId
	        							AND L.ObraLocalId != P.ObraLocalId
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.OSLocais>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.OS>, int>> GetOS(Guid tarefaGuid, int empresaId, DateTime? lastSync)
        {
            string script = @$"
		            SELECT L.ObraCronogramaTarefaLocalId
			             , L.ObraCronogramaTarefaId 
		            	 , L.ObraLocalId
		            	 , CASE 
	                        WHEN LEN(replace(RIGHT(Caminho,charindex('\',reverse(Caminho),1)+0),'\',''))>0
	                        	THEN replace(RIGHT(Caminho,charindex('\',reverse(Caminho),1)+0),'\','')
	                        ELSE Caminho
	                       END								    AS Local
			             , L.Andamento
			             , ISNULL(L.DataInicio, T.DataInicio)	AS DataInicio
			             , ISNULL(L.DataFim, T.DataFim)			AS DataFim
		            	 , L.Guid
		            	 , T.Guid                               AS TarefaGuid
		            	 , L.DataHoraAlteracao
                         , L.Checked                
			             , COUNT(1) OVER()					    AS Total
		              FROM ObraCronogramaTarefaLocal	L
		              JOIN ObraCronogramaTarefa			T	    ON L.ObraCronogramaTarefaId = T.ObraCronogramaTarefaId
		              JOIN ObraLocal					OL	    ON L.ObraLocalId = OL.ObraLocalId
		             WHERE L.EmpresaId = @EmpresaId
		               AND L.Checked = 1
                       AND T.Guid = @TarefaGuid
		               AND (L.DataHoraAlteracao > @LastSync
		                OR  @LastSync IS NULL)";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                tarefaGuid,
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.OS>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task PostOS(DTO.OS os, int empresaId, int usuarioId)
        {
            _dapperDatabaseHelper.CreateConnection();
            _dapperDatabaseHelper.CreateTransaction();

            string script = @"
             IF NOT EXISTS(
		     SELECT 1 
		       FROM ObraCronogramaTarefaLocal	L
			   JOIN ObraCronogramaTarefa			T ON L.ObraCronogramaTarefaId = T.ObraCronogramaTarefaId
 		      WHERE T.Guid = @TarefaGuid
		        AND L.ObraLocalId = @ObraLocalId)

             INSERT
		       INTO ObraCronogramaTarefaLocal
		          ( EmpresaId
		     	  , ObraCronogramaTarefaId
		     	  , ObraLocalId
		     	  , Checked
				  , Guid
		          , UsuarioIdInclusao  
		          , DataHoraInclusao  
		          , UsuarioIdAlteracao  
		          , DataHoraAlteracao)
	         SELECT @EmpresaId
		          , T.ObraCronogramaTarefaId
		     	  , @ObraLocalId
		     	  , 1
				  , @Guid
		          , @UsuarioId    
		          , GETDATE()
		          , @UsuarioId    
		          , GETDATE()
			   FROM ObraCronogramaTarefa T
			  WHERE T.Guid = @TarefaGuid
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                UsuarioId = usuarioId,
                os.TarefaGuid,
                os.ObraLocalId,
                os.guid
            });
            //
            await _dapperDatabaseHelper.Execute(script, dParams: dynamic);

            _dapperDatabaseHelper.CommitTransaction();
            _dapperDatabaseHelper.CloseConnection();
        }

        public async Task PutOS(DTO.OS os, int empresaId, int usuarioId)
        {
            _dapperDatabaseHelper.CreateConnection();
            _dapperDatabaseHelper.CreateTransaction();

            string script = @"
            UPDATE ObraCronogramaTarefaLocal
		       SET DataInicio = @DataInicio
		    	 , DataFim = @DataFim
		    	 , DataHoraAlteracao = GETDATE()
		    	 , UsuarioIdAlteracao = @UsuarioId
		      FROM ObraCronogramaTarefaLocal
		     WHERE EmpresaId = @EmpresaId
               AND Guid = @Guid
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                UsuarioId = usuarioId,
                os.DataInicio,
                os.DataFim,
                os.guid
            });
            //
            await _dapperDatabaseHelper.Execute(script, dParams: dynamic);

            _dapperDatabaseHelper.CommitTransaction();
            _dapperDatabaseHelper.CloseConnection();
        }

        #endregion

        #region Tarefa

        public async Task<Tuple<IEnumerable<DTO.TarefaLista>, int>> GetLista(int obraId, int empresaId, DateTime? lastSync, int? statusId, string? query)
        {
            string script = @$"
		            WITH Tarefas AS (
   			  	  SELECT T.Guid
					   , T.ObraCronogramaTarefaId	AS TarefaId
					   , T.Nome
					   , T.ObraId					
					   , O.Nome						AS Obra
					   , T.DataInicio
					   , T.DataFim
					   , ISNULL(T.Icone,'fas fa-tasks')	AS Icone
					   , A.Porcentagem
					   , CASE 
							WHEN A.Porcentagem = 0
								THEN 0
							WHEN A.Porcentagem BETWEEN 1 AND 99
								THEN 1
							WHEN A.Porcentagem = 100
								THEN 2
						 END						AS StatusId
   			  		   , COUNT (1) OVER()			AS Total
   			  	    FROM ObraCronogramaTarefa	T
					JOIN Obra					O	ON T.ObraId = O.ObraId
			   LEFT JOIN V_Tarefa_Andamento		A	ON T.ObraCronogramaTarefaId = A.ObraCronogramaTarefaId
				   WHERE T.EmpresaId = @EmpresaId
					 AND (T.ObraId = @ObraId
                      OR  @ObraId = 0)
					 AND (@StatusId IS NULL
                      OR  (@StatusId = 0 AND A.Porcentagem = 0)
					  OR  (@StatusId = 1 AND A.Porcentagem BETWEEN 1 AND 99)
					  OR  (@StatusId = 2 AND A.Porcentagem = 100))
					 AND (@Query IS NULL
					  OR  T.Nome LIKE @Query COLLATE SQL_Latin1_General_CP1_CI_AI)
					 AND (@LastSync IS NULL
					  OR  T.DataHoraAlteracao > @LastSync)
					   )
	              SELECT T.*
	                FROM Tarefas T
				ORDER BY T.Nome";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                ObraId = obraId,
                StatusId = statusId,
                Query = query is null ? null : $"%{query}%",
                LastSync = lastSync
            });
            //
            return await _dapperDatabaseHelper.QueryAll<DTO.TarefaLista>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.Tarefa>, int>> Get(Guid tarefaGuid, int empresaId)
        {
            string script = @$"
		            WITH Tarefas AS (
   			  	  SELECT T.Guid
					   , T.ObraCronogramaTarefaId	AS TarefaId
					   , T.Nome
					   , ISNULL(T.IndiceUnidade,0)	AS Quantidade
					   , T.ObraId					
					   , O.Nome						AS Obra
					   , T.ModeloServicoId
					   , S.Nome						AS ModeloServico
					   , T.GrupoCompraId
					   , G.Nome						AS GrupoCompra
					   , T.UnidadeMedidaId
					   , U.Nome						AS UnidadeMedida
					   , T.DataInicio
					   , T.DataFim
					   , T.Observacao
					   , ISNULL(T.Icone,'fas fa-tasks')	AS Icone
					   , A.Porcentagem
					   , CASE 
							WHEN A.Porcentagem = 0
								THEN 0
							WHEN A.Porcentagem BETWEEN 1 AND 99
								THEN 1
							WHEN A.Porcentagem = 100
								THEN 2
						 END						AS StatusId
                       , A.Aberta                   AS OSAberta
                       , A.Andamento                AS OSAndamento
                       , A.Concluidas               AS OSConcluidas
   			  		   , COUNT (1) OVER()			AS Total
   			  	    FROM ObraCronogramaTarefa	T
					JOIN Obra					O	ON T.ObraId = O.ObraId
			   LEFT JOIN V_Tarefa_Andamento		A	ON T.ObraCronogramaTarefaId = A.ObraCronogramaTarefaId
			   LEFT JOIN Servico				S	ON T.ModeloServicoId = S.ServicoId
			   LEFT JOIN GrupoCompra			G	ON T.GrupoCompraId = G.GrupoCompraId
			   LEFT JOIN UnidadeMedida			U	ON T.UnidadeMedidaId = U.UnidadeMedidaId
				   WHERE T.EmpresaId = @EmpresaId
					 AND T.Guid = @TarefaGuid
					   )
	              SELECT T.*
	                FROM Tarefas T
				ORDER BY T.Nome";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                TarefaGuid = tarefaGuid
            });
            //
            var result = await _dapperDatabaseHelper.QueryAll<DTO.Tarefa>(script, dParams: dynamic);

            return result;
        }

        public async Task Put(DTO.Tarefa tarefa, int empresaId, int usuarioId)
        {
            _dapperDatabaseHelper.CreateConnection();
            _dapperDatabaseHelper.CreateTransaction();

            string script = @"
                UPDATE ObraTarefa
            	   SET ObraId				= @ObraId
            		 , Nome					= ISNULL(@Nome, T.Nome) 
            		 , Quantidade			= @Quantidade
            		 , DataInicio			= @DataInicio
            		 , DataFim				= @DataFim
            		 , UsuarioIdAlteracao	= @UsuarioId
            		 , DataHoraAlteracao	= GETDATE()
            	  FROM ObraTarefa T
                 WHERE Guid = @Guid		  
            
            	UPDATE ObraCronogramaTarefa
            	   SET ObraId               = ISNULL(T.ObraId, @ObraId)
            		 , Nome					= ISNULL(@Nome, T.Nome) 
            		 , Icone				= ISNULL(NULLIF(TRIM(@Icone),''),'fa fa-file-o')
            		 , IndiceUnidade		= @Quantidade
            		 , ModeloServicoId		= @ModeloServicoId
            		 , DataInicio           = ISNULL(@DataInicio, GETDATE())
            		 , DataFim				= ISNULL(@DataFim, GETDATE())
            		 , UsuarioIdAlteracao	= @UsuarioId
            		 , DataHoraAlteracao	= GETDATE()
            		 , UnidadeMedidaId		= @UnidadeMedidaId
            		 , Observacao			= @Observacao
            	  FROM ObraCronogramaTarefa T
            	 WHERE T.Guid = @Guid;


                    WITH Tarefas AS (
   			  	  SELECT T.Guid
					   , T.ObraCronogramaTarefaId	AS TarefaId
					   , T.Nome
					   , ISNULL(T.IndiceUnidade,0)	AS Quantidade
					   , T.ObraId					
					   , O.Nome						AS Obra
					   , T.ModeloServicoId
					   , S.Nome						AS ModeloServico
					   , T.GrupoCompraId
					   , G.Nome						AS GrupoCompra
					   , T.UnidadeMedidaId
					   , T.Nome						AS UnidadeMedida
					   , T.DataInicio
					   , T.DataFim
					   , T.Observacao
					   , ISNULL(T.Icone,'fas fa-tasks')	AS Icone
					   , A.Porcentagem
					   , CASE 
							WHEN A.Porcentagem = 0
								THEN 0
							WHEN A.Porcentagem BETWEEN 1 AND 99
								THEN 1
							WHEN A.Porcentagem = 100
								THEN 2
						 END						AS StatusId
   			  		   , COUNT (1) OVER()			AS Total
   			  	    FROM ObraCronogramaTarefa	T
					JOIN Obra					O	ON T.ObraId = O.ObraId
			   LEFT JOIN V_Tarefa_Andamento		A	ON T.ObraCronogramaTarefaId = A.ObraCronogramaTarefaId
			   LEFT JOIN Servico				S	ON T.ModeloServicoId = S.ServicoId
			   LEFT JOIN GrupoCompra			G	ON T.GrupoCompraId = G.GrupoCompraId
			   LEFT JOIN UnidadeMedida			U	ON T.UnidadeMedidaId = U.UnidadeMedidaId
				   WHERE T.EmpresaId = @EmpresaId
					 AND T.Guid = @Guid
					   )
	              SELECT T.*
	                FROM Tarefas T
				ORDER BY T.Nome
";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                UsuarioId = usuarioId,
                Guid = tarefa.guid,
                tarefa.Nome,
                tarefa.Quantidade,
                tarefa.ObraId,
                tarefa.DataInicio,
                tarefa.DataFim,
                tarefa.ModeloServicoId,
                tarefa.UnidadeMedidaId,
                tarefa.Observacao,
                tarefa.Icone
            });
            //
            await _dapperDatabaseHelper.Execute(script, dParams: dynamic);

            _dapperDatabaseHelper.CommitTransaction();
            _dapperDatabaseHelper.CloseConnection();

        }

        public async Task Post(DTO.Tarefa tarefa, int empresaId, int usuarioId)
        {
            _dapperDatabaseHelper.CreateConnection();
            _dapperDatabaseHelper.CreateTransaction();

            string script = @"
               DECLARE @MaxTarefaId			    INT 
		             , @MaxSequencia			INT 
		             , @MaxCronogramaTarefaId	INT
		             , @Name					VARCHAR(100)		  
		             , @ExecucaoInicio			DATETIME = GETDATE();

          	    SELECT @Name = 'Nova tarefa #' + CONVERT(VARCHAR(20),COUNT(ObraId) + 1)
            	  FROM ObraCronogramaTarefa
            	 WHERE ObraId = @ObraId 
            
            	SELECT @Guid										AS Guid
            		 , ISNULL(@Nome, @Name)							AS Nome
            		 , @Icone										AS Icone
            		 , '00000000-0000-0000-0000-000000000000'		AS GuidPai 
            		 , ISNULL(@DataInicio,GETDATE())				AS DataInicio
            	     , ISNULL(@DataFim,DATEADD(day,1,getdate()))	AS DataFim
            		 , @ModeloServicoId								AS ModeloServicoId
            		 , @UnidadeMedidaId								AS UnidadeMedidaId
            		 , @Quantidade									AS IndiceUnidade
            		 , @Observacao                                  AS Observacao
            	  INTO #Dados
            
            	SELECT @MaxSequencia  = ISNULL(MAX(Sequencia),0)+1 
            	  FROM ObraTarefa 
            	 WHERE ObraId = @ObraId
            
            	INSERT
            	  INTO ObraTarefa
            		 ( EmpresaId
            		 , ObraId
            		 , Nome
            		 , Quantidade
            		 , Sequencia
            		 , DataInicio
            		 , DataFim
            		 , UsuarioIdInclusao
            		 , DataHoraInclusao
            		 , UsuarioIdAlteracao
            		 , DataHoraAlteracao
            		 , Guid
            		 )
            	SELECT @EmpresaId
            		 , @ObraId
            		 , D.Nome
            		 , D.IndiceUnidade
            		 , @MaxSequencia
            		 , D.DataInicio
            		 , D.DataFim
            		 , @UsuarioId
            		 , @ExecucaoInicio
            		 , @UsuarioId
            		 , @ExecucaoInicio
            		 , D.Guid
            	  FROM #Dados	D
            
            	   SET @MaxTarefaId = SCOPE_IDENTITY()
            
            	INSERT
            	  INTO ObraCronogramaTarefa
            		 ( EmpresaId   
            		 , ObraId      
            		 , Guid                                 
            		 , GuidPai                              
            		 , Nome 
            		 , Icone
            		 , IndiceUnidade
            		 , ModeloServicoId
            		 , DataInicio              
            		 , DataFim  
            		 , Duracao
            		 , DuracaoTipo
            		 , UsuarioIdInclusao 
            		 , DataHoraInclusao        
            		 , UsuarioIdAlteracao 
            		 , DataHoraAlteracao
            		 , indice
            		 , UnidadeMedidaId
            		 , Observacao
            		 )
            	SELECT @EmpresaId
            		 , @ObraId
            		 , D.Guid
            		 , GuidPai
            		 , D.Nome
            		 , ISNULL(NULLIF(TRIM(D.Icone),''),'fa fa-file-o')
            		 , ISNULL(D.IndiceUnidade, 0)
            		 , D.ModeloServicoId
            		 , D.DataInicio
            		 , D.DataFim
            		 , DATEDIFF(DAY,D.DataInicio,D.DataFim)   --D.Duracao
            		 , 'd' --D.DuracaoTipo
            		 , @UsuarioId
            		 , @ExecucaoInicio
            		 , @UsuarioId
            		 , @ExecucaoInicio
            		 , @MaxSequencia
            		 , ISNULL(D.UnidadeMedidaId, S.UnidadeMedidaId)
            		 , ISNULL(D.Observacao, S.Observacao)
            	  FROM #Dados	D
             LEFT JOIN Servico	S ON D.ModeloServicoId = S.ServicoId
            
            	   SET @MaxCronogramaTarefaId = SCOPE_IDENTITY()
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                EmpresaId = empresaId,
                UsuarioId = usuarioId,
                Guid = tarefa.guid,
                tarefa.ObraId,
                tarefa.Nome,
                tarefa.DataInicio,
                tarefa.DataFim,
                tarefa.ModeloServicoId,
                tarefa.UnidadeMedidaId,
                tarefa.Quantidade,
                tarefa.Icone,
                tarefa.Observacao
            });
            //
            await _dapperDatabaseHelper.Execute(script, dParams: dynamic);

            _dapperDatabaseHelper.CommitTransaction();
            _dapperDatabaseHelper.CloseConnection();
        }

        #endregion

        public async Task<Tuple<IEnumerable<DTO.Arquivo>, int>> Arquivos(Guid tarefaGuid, int empresaId, DateTime? lastSync)
        {
            string script = @$"
		    SELECT T.EmpresaId
		    	 , T.ObraCronogramaTarefaId
                 , T.Guid                           AS TarefaGuid
		    	 , M.NomeArquivo					AS Nome
		    	 , M.Url							AS Caminho
		    	 , M.Extensao
                 , A.Guid
		    	 , A.DataHoraAlteracao
		      FROM ObraCronogramaTarefa			T
		      JOIN ObraCronogramaTarefaArquivo	A	ON T.ObraCronogramaTarefaId = A.ObraCronogramaTarefaId
			  JOIN Media						M	ON A.MediaGuid= M.Guid
		     WHERE T.EmpresaId = @EmpresaId
		       AND T.Guid = @TarefaGuid
		       AND (A.DataHoraAlteracao > @LastSync
		    	OR @LastSync IS NULL)";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                empresaId,
                tarefaGuid,
                lastSync
            });
            //
            var result = await _dapperDatabaseHelper.QueryAll<DTO.Arquivo>(script, dParams: dynamic);

            return result;
        }
    }
}
