﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTO = Tskplan.Models.AgendaEntrega.DTO;

namespace Tskplan.Repositories.AgendaEntrega
{
    public interface IAgendaEntregaRepository
    {
        Task<Tuple<IEnumerable<DTO.Calendario>, int>> GetCalendario(int mes, int ano, int empresaId, DateTime? lastSync);

        Task<Tuple<IEnumerable<DTO.Produto>, int>> GetProdutos(Guid agendaGuid, int empresaId, DateTime? lastSync);

        Task<int> GerarEntrega(Guid pedidoEntregaGuid, int empresaId, int usuarioId);
    }
}
