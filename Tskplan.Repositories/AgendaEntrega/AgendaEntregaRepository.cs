﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tskplan.Data;
using DTO = Tskplan.Models.AgendaEntrega.DTO;

namespace Tskplan.Repositories.AgendaEntrega
{
    public class AgendaEntregaRepository : IAgendaEntregaRepository
    {
        private readonly IDapperDatabaseHelper _dapperDatabaseHelper;
        private readonly ILogger<AgendaEntregaRepository> _logger;

        public AgendaEntregaRepository(IDapperDatabaseHelper dapperDatabaseHelper, ILogger<AgendaEntregaRepository> logger)
        {
            _dapperDatabaseHelper = dapperDatabaseHelper ?? throw new ArgumentNullException(nameof(dapperDatabaseHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<DTO.Calendario>, int>> GetCalendario(int mes, int ano, int empresaId, DateTime? lastSync)
        {
            string script = @$"
                WITH Pedidos AS (
			  SELECT E.PedidoCompraEntregaId
	        	   , E.PedidoCompraId
	        	   , E.DataEntrega
	        	   , E.Observacao
	        	   , ISNULL(F.NomeFantasia,F.RazaoSocial)	AS Fornecedor
	        	   , P.PedidoCompraStatusId					AS StatusId
	        	   , S.Nome									AS Status
                   , DAY(DataEntrega)                      AS Dia
                   , MONTH(DataEntrega)                     AS Mes
                   , YEAR(DataEntrega)                      AS Ano
				   , O.ObraId
				   , O.Nome									AS Obra
	        	   , E.Guid
	        	   , E.DataHoraAlteracao
	        	FROM PedidoCompraEntrega	E
	        	JOIN PedidoCompra			P ON E.PedidoCompraId = P.PedidoCompraId
	        	JOIN PedidoCompraStatus		S ON P.PedidoCompraStatusId = S.PedidoCompraStatusId
	        	JOIN Fornecedor				F ON P.FornecedorId = F.FornecedorId
		   LEFT JOIN V_PedidoCompra_Obra    PO	ON P.PedidoCompraId = PO.PedidoCompraId
		   LEFT JOIN Obra					O	ON PO.ObraId = O.ObraId
	           WHERE E.EmpresaId = @EmpresaId
                   )
              SELECT P.*
                FROM Pedidos P
               WHERE Mes = @Mes
	        	 AND Ano = @Ano
	        	 AND (@LastSync IS NULL 
	        	  OR DataHoraAlteracao > @LastSync)";

            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                mes,
                ano,
                empresaId,
                lastSync
            });

            return await _dapperDatabaseHelper.QueryAll<DTO.Calendario>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<Tuple<IEnumerable<DTO.Produto>, int>> GetProdutos(Guid agendaGuid, int empresaId, DateTime? lastSync)
        {
            string script = @$"
                SELECT I.PedidoCompraEntregaItemId
		        	 , I.PedidoCompraEntregaId
		        	 , I.ProdutoId
		        	 , P.Nome
		        	 , I.Quantidade
		        	 , E.Guid                       AS AgendaGuid
		        	 , I.Guid
		        	 , I.DataHoraAlteracao
		          FROM PedidoCompraEntregaItem	I
		          JOIN PedidoCompraEntrega		E	ON I.PedidoCompraEntregaId = E.PedidoCompraEntregaId
		          JOIN Produto					P	ON I.ProdutoId = P.ProdutoId
		         WHERE E.EmpresaId = @EmpresaId
		           AND E.Guid = @Guid
	               AND (@LastSync IS NULL 
	                OR I.DataHoraAlteracao > @LastSync)";

            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                guid = agendaGuid,
                empresaId,
                lastSync
            });

            return await _dapperDatabaseHelper.QueryAll<DTO.Produto>(script, dParams: dynamic, useTransaction: true);
        }

        public async Task<int> GerarEntrega(Guid pedidoEntregaGuid, int empresaId, int usuarioId)
        {
            _dapperDatabaseHelper.CreateConnection();
            _dapperDatabaseHelper.CreateTransaction();

            string script = @"
               DECLARE @MaxId INT;

				INSERT 
				  INTO Entrega
				     ( EmpresaId
					 , PedidoCompraId
					 , DataEntrega
					 , Observacao
					 , UsuarioIdInclusao
					 , DataHoraInclusao
					 , UsuarioIdAlteracao
					 , DataHoraAlteracao)
				SELECT @EmpresaId
					 , PE.PedidoCompraId
					 , PE.DataEntrega
					 , CONCAT('Entrega gerada atraves da agenda de entregas do pedido de compra do dia ', convert(varchar, PE.DataEntrega, 103),'.')
					 , @UsuarioId
					 , GETDATE()
					 , @UsuarioId
					 , GETDATE()
				  FROM PedidoCompraEntrega PE
				 WHERE PE.Guid = @PedidoEntregaGuid

				SELECT @MaxId = SCOPE_IDENTITY()

				INSERT
				  INTO EntregaProduto
				     ( EmpresaId
					 , EntregaId
					 , PedidoCompraItemId
					 , ProdutoId
					 , Quantidade
					 , Observacao
					 , UsuarioIdInclusao
					 , DataHoraInclusao
					 , UsuarioIdAlteracao
					 , DataHoraAlteracao)
				SELECT @EmpresaId
					 , @MaxId
					 , P.PedidoCompraItemId
					 , I.ProdutoId
					 , I.Quantidade
					 , 'Produto inserido na entrega gerada atraves da agenda de entregas do pedido de compra.'
					 , @UsuarioId
					 , GETDATE()
					 , @UsuarioId
					 , GETDATE()
				  FROM PedidoCompraEntregaItem	I
				  JOIN PedidoCompraEntrega		E	ON I.PedidoCompraEntregaId = E.PedidoCompraEntregaId
				  JOIN PedidoCompraItem			P	ON E.PedidoCompraId = P.PedidoCompraId
				  JOIN CotacaoFinalizacao		C	ON P.CotacaoFinalizacaoId = C.CotacaoFinalizacaoId
												   AND C.ProdutoId = I.ProdutoId
				 WHERE E.Guid = @PedidoEntregaGuid

				SELECT SCOPE_IDENTITY()
            ";
            //
            DynamicParameters dynamic = new DynamicParameters();
            dynamic.AddDynamicParams(new
            {
                empresaId,
                usuarioId,
                pedidoEntregaGuid,
            });

			var entregaId = await _dapperDatabaseHelper.ExecuteScalar<int>(script, dParams: dynamic);

            _dapperDatabaseHelper.CommitTransaction();
            _dapperDatabaseHelper.CloseConnection();

			return entregaId;
		}
	}
}
