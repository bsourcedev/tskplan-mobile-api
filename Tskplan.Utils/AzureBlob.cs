﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using Tskplan.Models.Configuration;

namespace Tskplan.Utils
{
    public class AzureBlob
    {
        private static ConnectionStringsSettings _connectionStringsSettings;

        public static async Task<string> UploadBlobAsync(MemoryStream memoryStream, string imageName, ConnectionStringsSettings connectionStringsSettings)
        {
            _connectionStringsSettings = connectionStringsSettings;
            try
            {
                string blobUrl = string.Empty;
                string token = GetAccountSASToken();
                Uri container = GetContainerUri("media");
                StorageCredentials storageCredentials = new StorageCredentials(token);
                CloudBlobContainer blobContainer = new CloudBlobContainer(container, storageCredentials);

                try
                {
                    await blobContainer.CreateIfNotExistsAsync();
                }
                catch (StorageException ex)
                {
                    throw ex;
                }

                CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(imageName);

                blockBlob.Properties.ContentType = "image/png";
                await blockBlob.UploadFromStreamAsync(memoryStream);

                blobUrl = $"{container.AbsoluteUri}/{imageName}{token}";

                return blobUrl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static Uri GetContainerUri(string containerName)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_connectionStringsSettings.AzureDefault);

            return storageAccount.CreateCloudBlobClient().GetContainerReference(containerName).Uri;
        }

        private static string GetAccountSASToken()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_connectionStringsSettings.AzureDefault);

            SharedAccessAccountPolicy policy = new SharedAccessAccountPolicy()
            {
                Permissions = SharedAccessAccountPermissions.Read | SharedAccessAccountPermissions.Write | SharedAccessAccountPermissions.List | SharedAccessAccountPermissions.Create | SharedAccessAccountPermissions.Delete,
                Services = SharedAccessAccountServices.Blob,
                ResourceTypes = SharedAccessAccountResourceTypes.Container | SharedAccessAccountResourceTypes.Object,
                SharedAccessExpiryTime = DateTime.UtcNow.AddYears(30),
                Protocols = SharedAccessProtocol.HttpsOrHttp
            };

            string sasToken = storageAccount.GetSharedAccessSignature(policy);

            return sasToken;
        }
    }
}
