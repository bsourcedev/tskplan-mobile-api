﻿using Tskplan.Models.Response;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Data.SqlClient;
using System.Text;
using System.Security.Authentication;

namespace Tskplan.Utils
{
    public static class ExceptionHandler
    {
        private const string ERROR_TYPE_APPLICATION_VALIDATION = "application validation";
        private const string ERROR_TYPE_MODEL_VALIDATION = "model validation";
        private const string ERROR_TYPE_SERVER_GENERAL = "server general";
        private const string ERROR_TYPE_DATABASE_VALIDATION = "database validation";
        private const string ERROR_TYPE_DATABASE_GENERAL = "database general";
        private const int HTTP_STATUS_400_BAD_REQUEST = 400;
        private const int HTTP_STATUS_401_NOT_AUTHORIZED = 401;
        private const int HTTP_STATUS_500_INTERNAL_SERVER_ERROR = 500;

        public static ErrorResponseData Handle(Exception ex)
        {
            ErrorResponseData errorData = new ErrorResponseData();
            if (ex is ApplicationException)
            {
                errorData.StatusCode = HTTP_STATUS_400_BAD_REQUEST;
                errorData.Type = ERROR_TYPE_APPLICATION_VALIDATION;
                errorData.Message = ex.Message;
                errorData.ExceptionDetails = ex;
            }
            else if (ex is SqlException)
            {
                SqlException sqlEx = (SqlException)ex;
                errorData.StatusCode = sqlEx.Number >= 5000 ? HTTP_STATUS_400_BAD_REQUEST : HTTP_STATUS_500_INTERNAL_SERVER_ERROR;
                errorData.ErrorCode = sqlEx.ErrorCode;
                errorData.ErrorNumber = sqlEx.Number;
                errorData.Type = sqlEx.ErrorCode >= 5000 ? ERROR_TYPE_DATABASE_VALIDATION : ERROR_TYPE_DATABASE_GENERAL;
                errorData.Message = ex.Message;
                errorData.ExceptionDetails = ex;
            }
            else if (ex is AuthenticationException)
            {
                errorData.StatusCode = HTTP_STATUS_401_NOT_AUTHORIZED;
                errorData.Message = ex.Message;
            }
            else if (ex is Exception)
            {
                errorData.StatusCode = HTTP_STATUS_500_INTERNAL_SERVER_ERROR;
                errorData.Type = ERROR_TYPE_SERVER_GENERAL;
                errorData.Message = ex.Message;
                errorData.ExceptionDetails = ex;
            }
            return errorData;
        }

        public static ErrorResponseData Handle(ModelStateDictionary modelState)
        {
            ErrorResponseData errorData = new ErrorResponseData();
            StringBuilder strBuilder = new StringBuilder();
            //
            foreach (string key in modelState.Keys)
            {
                foreach (ModelError error in modelState[key].Errors)
                {
                    strBuilder.AppendLine(error.ErrorMessage);
                }
            }
            //
            errorData.StatusCode = HTTP_STATUS_400_BAD_REQUEST;
            errorData.Type = ERROR_TYPE_MODEL_VALIDATION;
            errorData.Message = strBuilder.ToString();
            errorData.OtherDetails = modelState;
            return errorData;
        }
    }
}
