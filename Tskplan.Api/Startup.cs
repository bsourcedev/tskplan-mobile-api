using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Tskplan.Models.Configuration;
using Ser = Tskplan.Services;
using Rep = Tskplan.Repositories;
using Tskplan.Data;
using Tskplan.Models.Response;
using Tskplan.Api.Attributes;
using Tskplan.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Net;
using System.Text;
using Tskplan.Api.Logger;
using Microsoft.AspNetCore.HttpOverrides;

namespace Tskplan.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        //private void AddMvc(IServiceCollection services)
        //{
        //    services.AddMvc(options =>
        //    {
        //        options.Filters.Add(new ValidateModelAttribute());
        //        options.Filters.Add(new ProducesAttribute("application/json"));
        //        options.Filters.Add(new ProducesResponseTypeAttribute(typeof(ErrorResponseData), StatusCodes.Status400BadRequest));
        //        options.Filters.Add(new ProducesResponseTypeAttribute(typeof(ErrorResponseData), StatusCodes.Status500InternalServerError));
        //    }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
        //    .AddJsonOptions(options =>
        //    {
        //        options.JsonSerializerOptions.PropertyNamingPolicy = null;
        //        options.JsonSerializerOptions.DictionaryKeyPolicy = null;
        //    });
        //}
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureAppSettingsObjects(services);
            AddLogicInstances(services);
            services.AddControllers(options =>
            {
                options.Filters.Add(new ValidateModelAttribute());
                options.Filters.Add(new ProducesAttribute("application/json"));
                options.Filters.Add(new ProducesResponseTypeAttribute(typeof(ErrorResponseData), StatusCodes.Status400BadRequest));
                options.Filters.Add(new ProducesResponseTypeAttribute(typeof(ErrorResponseData), StatusCodes.Status500InternalServerError));
            })
            .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
                options.JsonSerializerOptions.DictionaryKeyPolicy = null;
                options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            services.AddDistributedRedisCache(x =>
            {
                x.Configuration = "http://localhost:6379";
                x.InstanceName = "";
            });

            services.AddSession(x =>
            {
                x.IdleTimeout = TimeSpan.FromMinutes(60);
            });

            Microsoft.IdentityModel.Logging.IdentityModelEventSource.ShowPII = true;

            var appSettings = Configuration.GetSection("AppSettings").Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false
                };
            });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                //Previne que o Model seja validado antes de acessar o Controller. Isso fazia com que o ValidateModelAttribute n�o fosse disparado
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        public void ConfigureAppSettingsObjects(IServiceCollection services)
        {
            services.Configure<ConnectionStringsSettings>(Configuration.GetSection("ConnectionStrings"));
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
        }

        private void AddLogicInstances(IServiceCollection services)
        {
            services.AddTransient<IDapperDatabaseHelper, SqlDapperDatabaseHelper>();
            //
            services.AddScoped<Ser.Session.ISessionService, Ser.Session.SessionService>();
            services.AddScoped<Rep.Session.ISessionRepository, Rep.Session.SessionRepository>();
            //
            services.AddScoped<Ser.Obra.IObraService, Ser.Obra.ObraService>();
            services.AddScoped<Rep.Obra.IObraRepository, Rep.Obra.ObraRepository>();
            //
            services.AddScoped<Ser.Checklist.IChecklistService, Ser.Checklist.ChecklistService>();
            services.AddScoped<Rep.Checklist.IChecklistRepository, Rep.Checklist.ChecklistRepository>();
            //
            services.AddScoped<Ser.Tarefa.ITarefaService, Ser.Tarefa.TarefaService>();
            services.AddScoped<Rep.Tarefa.ITarefaRepository, Rep.Tarefa.TarefaRepository>();
            //
            services.AddScoped<Ser.Media.IMediaService, Ser.Media.MediaService>();
            services.AddScoped<Rep.Media.IMediaRepository, Rep.Media.MediaRepository>();
            //
            services.AddScoped<Ser.Entrega.IEntregaService, Ser.Entrega.EntregaService>();
            services.AddScoped<Rep.Entrega.IEntregaRepository, Rep.Entrega.EntregaRepository>();
            //
            services.AddScoped<Ser.AgendaEntrega.IAgendaEntregaService, Ser.AgendaEntrega.AgendaEntregaService>();
            services.AddScoped<Rep.AgendaEntrega.IAgendaEntregaRepository, Rep.AgendaEntrega.AgendaEntregaRepository>();
            //
            services.AddScoped<Ser.Global.IGlobalService, Ser.Global.GlobalService>();
            services.AddScoped<Rep.Global.IGlobalRepository, Rep.Global.GlobalRepository>();
            //
            services.AddScoped<Ser.Chat.IChatService, Ser.Chat.ChatService>();
            services.AddScoped<Rep.Chat.IChatRepository, Rep.Chat.ChatRepository>();
        }

        private void ConfigureExceptionHandler(IApplicationBuilder app)
        {
            // Adiciona um middleware para capturar os erros
            app.UseExceptionHandler(appError =>
            {
                // Adiciona um middleware final para a requisi��o
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        ErrorResponseData errorData = ExceptionHandler.Handle(contextFeature.Error);
                        context.Response.StatusCode = errorData.StatusCode;
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(errorData));
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            ConfigureExceptionHandler(app);

            //app.UseHttpsRedirection();
            app.UseRouting();
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseAuthentication();
            app.UseAuthorization();
            //app.UseCors("CorsPolicy");
            //Todo Validar esta config
            app.UseCors(
                options => options.SetIsOriginAllowed(x => _ = true).AllowAnyMethod().AllowAnyHeader().AllowCredentials()
            );
            //
            app.UseSession();

            app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllerRoute(name: "default", pattern: "v1/{controller}/{action}/{id?}");
                });

            loggerFactory.AddContext(LogLevel.Debug);
        }
    }


    
}
