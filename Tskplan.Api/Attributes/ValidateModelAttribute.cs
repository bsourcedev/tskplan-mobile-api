﻿using Tskplan.Models.Response;
using Tskplan.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Tskplan.Api.Attributes
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                ErrorResponseData errorData = ExceptionHandler.Handle(context.ModelState);
                context.Result = new BadRequestObjectResult(errorData);
            }
        }
    }
}
