﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DTO = Tskplan.Models.Chat.DTO;
using Tskplan.Models.Response;
using Microsoft.AspNetCore.Cors;
using Tskplan.Services.Checklist;
using System.Security.Claims;
using Tskplan.Services.Chat;
using Tskplan.Models.Chat.DTO;
using Newtonsoft.Json;

namespace Tskplan.Api.Controllers
{
    [Route("v1/chat")]
    //[Authorize]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IChatService _chatService;
        private readonly ILogger<ChatController> _logger;

        public ChatController(IChatService chatService, ILogger<ChatController> logger)
        {
            _chatService = chatService ?? throw new ArgumentNullException(nameof(chatService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Get(int obraCronogramaTarefaLocalId, int obraCronogramaTarefaChecklistId, DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            int usuarioId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            Tuple<IEnumerable<DTO.Chat>, int> tuple = await _chatService.Get(usuarioId, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("Servers")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetServers(bool todosServers = false)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);

            Tuple<IEnumerable<DTO.Servers>, int> tuple = await _chatService.GetServers(todosServers, empresaId);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpPost]
        [Route("Message")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> PostMessage(MessageData message)
        {
            await _chatService.PostMessage(message);

            return Ok();
        }

        [HttpGet]
        [Route("MessageBuffer")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetMessageBuffer(Guid channel)
        {
            Tuple<IEnumerable<MessageData>, int> tuple = await _chatService.ObtemMensagensDoCanal(channel);

            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("UserChannels")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetUserChannels(string usuario)
        {
            Tuple<IEnumerable<ChannelData>, int> tuple = await _chatService.ObtemCanaisDoUsuario(usuario);

            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpPost]
        [Route("TokenReg")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> PostTokenReg(string token)
        {
            int usuarioId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            await _chatService.RegistrarToken(token, usuarioId);

            return Ok();
        }

        [HttpPost]
        [Route("UserChannelReg")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> PostUserChannelReg(Guid channel)
        {
            int usuarioId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            await _chatService.RegistrarUsuarioAoCanal(channel, usuarioId);

            return Ok();
        }

        [HttpGet]
        [Route("NewChannels")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetNewChannels(int obraId = 0, string pesquisa = "", string usuario = "")
        {
            Tuple<IEnumerable<ChannelData>, int> tuple = await _chatService.ObtemCanais(obraId, pesquisa);

            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }
    }
}