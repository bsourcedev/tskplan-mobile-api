﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DTO = Tskplan.Models.Tarefa.DTO;
using Tskplan.Models.Response;
using Microsoft.AspNetCore.Cors;
using Tskplan.Services.Tarefa;
using System.Security.Claims;

namespace Tskplan.Api.Controllers
{
    [Route("v1/tarefa")]
    //[EnableCors]
    [Authorize]
    [ApiController]
    public class TarefaController : ControllerBase
    {
        private readonly ITarefaService _tarefaService;
        private readonly ILogger<TarefaController> _logger;

        public TarefaController(ITarefaService tarefaService, ILogger<TarefaController> logger)
        {
            _tarefaService = tarefaService ?? throw new ArgumentNullException(nameof(tarefaService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #region OS

        [HttpGet]
        [Route("OS")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetOS(Guid tarefaGuid, DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.OS>, int> tuple = await _tarefaService.GetOS(tarefaGuid, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("OS/locais")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetOSLocais(DateTime? lastSync, int obraId = 0, int tarefaId = 0)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.OSLocais>, int> tuple = await _tarefaService.GetOSLocais(obraId, tarefaId, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("OS/locais/auxiliar")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetOSLocaisAuxiliar(DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.OSLocais>, int> tuple = await _tarefaService.GetOSLocaisAuxiliar(empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpPost]
        [Route("OS")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> InsertOS([FromBody] DTO.OS os)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            int userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            await _tarefaService.PostOS(os, empresaId, userId);

            return Ok();
        }

        [HttpPut]
        [Route("OS")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> UpdateOS([FromBody] DTO.OS os)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            int userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            await _tarefaService.PutOS(os, empresaId, userId);

            return Ok();
        }

        [HttpGet]
        [Route("listaLocais")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetListaLocais(int obraId, int obraLocalId, DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.TarefaListaLocais>, int> tuple = await _tarefaService.GetListaLocais(obraId, obraLocalId, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        #endregion

        #region Tarefa

        [HttpGet]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Get(Guid tarefaGuid)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Tarefa>, int> tuple = await _tarefaService.Get(tarefaGuid, empresaId);

            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpPut]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Update([FromBody] DTO.Tarefa tarefa)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            await _tarefaService.Put(tarefa, empresaId, userId);

            Tuple<IEnumerable<DTO.Tarefa>, int> tuple = await _tarefaService.Get(tarefa.guid, empresaId);

            return Ok(new DefaultResponseData(tuple.Item1.ToList(), 1));
        }

        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Insert([FromBody] DTO.Tarefa tarefa)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            int userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            await _tarefaService.Post(tarefa, empresaId, userId);

            Tuple<IEnumerable<DTO.Tarefa>, int> tuple = await _tarefaService.Get(tarefa.guid, empresaId);

            return Ok(new DefaultResponseData(tuple.Item1.ToList(), 1));
        }

        [HttpGet]
        [Route("lista")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetLista(int? obraId, DateTime? lastSync, int? statusId, string? query)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.TarefaLista>, int> tuple = await _tarefaService.GetLista(obraId ?? 0, empresaId, lastSync, statusId, query);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        #endregion

        [HttpGet]
        [Route("arquivos")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetArquivos(Guid? tarefaGuid, DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Arquivo>, int> tuple = await _tarefaService.Arquivos(tarefaGuid ?? Guid.Empty, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }
    }
}