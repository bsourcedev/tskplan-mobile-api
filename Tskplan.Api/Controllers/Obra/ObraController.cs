﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DTO = Tskplan.Models.Obra.DTO;
using Tskplan.Models.Response;
using Microsoft.AspNetCore.Cors;
using Tskplan.Services.Obra;
using Newtonsoft.Json;

namespace Tskplan.Api.Controllers
{
    [Route("v1/obra")]
    [Authorize]
    [ApiController]
    public class ObraController : ControllerBase
    {
        private readonly IObraService _obraService;
        private readonly ILogger<ObraController> _logger;

        public ObraController(IObraService obraService, ILogger<ObraController> logger)
        {
            _obraService = obraService ?? throw new ArgumentNullException(nameof(obraService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Get(DateTime ? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Entrega>, int> tuple = await _obraService.Get(empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("local")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetLocais(DateTime? lastSync, int obraId =0)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.ObraLocal>, int> tuple = await _obraService.GetLocais(obraId, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }
    }
}