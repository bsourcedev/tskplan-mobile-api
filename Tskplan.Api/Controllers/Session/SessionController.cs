﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Tskplan.Services.Session;
using Tskplan.Models.Response;
using Tskplan.Models.Session.ViewModel;
using Microsoft.AspNetCore.Cors;
using Tskplan.Models.Session.DTO;

namespace Tskplan.Api.Controllers
{
    [Route("v1/session")]
    [ApiController]
    public class SessionController : ControllerBase
    {
        private readonly ISessionService _sessionService;
        private readonly ILogger<SessionController> _logger;

        public SessionController(ISessionService sessionService, ILogger<SessionController> logger)
        {
            _sessionService = sessionService ?? throw new ArgumentNullException(nameof(sessionService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<DefaultResponseData>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(IEnumerable<DefaultResponseData>), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Post(SessionPost item)
        {
            Session session = await _sessionService.Create(item);
            HttpContext.Session.SetString("token", session.Token);
            return Ok(session);
        }
    }
}