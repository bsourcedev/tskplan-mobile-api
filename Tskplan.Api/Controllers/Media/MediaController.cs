﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Tskplan.Models.Response;
using Tskplan.Models.Media.ViewModel;
using Tskplan.Services.Media;
using System.Security.Claims;

namespace Tskplan.Api.Controllers.Media
{
    [Route("v1/media")]
    [Authorize]
    [ApiController]
    public class MediaController : ControllerBase
    {
        private readonly IMediaService _mediaService;
        private readonly ILogger<MediaController> _logger;

        public MediaController(IMediaService mediaService, ILogger<MediaController> logger)
        {
            _mediaService = mediaService ?? throw new ArgumentNullException(nameof(mediaService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(IEnumerable<DefaultResponseData>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(IEnumerable<DefaultResponseData>), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Post(MediaPost item)
        {
            item.EmpresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            item.UsuarioId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            return Ok(await _mediaService.UpdateImage(item));
        }
    }
}