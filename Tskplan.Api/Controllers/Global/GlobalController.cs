﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Tskplan.Models.Response;
using DTO = Tskplan.Models.Global.DTO;
using Tskplan.Services.Global;

namespace Tskplan.Api.Controllers.Global
{
    [Route("v1/global")]
    [Authorize]
    [ApiController]
    public class GlobalController : ControllerBase
    {
        private readonly IGlobalService _globalService;
        private readonly ILogger<GlobalController> _logger;

        public GlobalController(IGlobalService globalService, ILogger<GlobalController> logger)
        {
            _globalService = globalService ?? throw new ArgumentNullException(nameof(globalService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        [Route("UnidadeMedida")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetUnidadeMedida(DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.UnidadeMedida>, int> tuple = await _globalService.GetUnidadeMedida(empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("Obra")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetObra(DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Obra>, int> tuple = await _globalService.GetObra(empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("ModeloServico")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetModeloServico(DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.ModeloServico>, int> tuple = await _globalService.GetModeloServico(empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }
    }
}