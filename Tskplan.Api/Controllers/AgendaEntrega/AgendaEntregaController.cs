﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DTO = Tskplan.Models.AgendaEntrega.DTO;
using Tskplan.Models.Response;
using Microsoft.AspNetCore.Cors;
using Tskplan.Services.AgendaEntrega;
using System.Security.Claims;

namespace Tskplan.Api.Controllers
{
    [Route("v1/agendaEntrega")]
    [Authorize]
    [ApiController]
    public class AgendaEntregaController : ControllerBase
    {
        private readonly IAgendaEntregaService _agendaEntregaService;
        private readonly ILogger<AgendaEntregaController> _logger;

        public AgendaEntregaController(IAgendaEntregaService agendaEntregaService, ILogger<AgendaEntregaController> logger)
        {
            _agendaEntregaService = agendaEntregaService ?? throw new ArgumentNullException(nameof(agendaEntregaService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        [Route("calendario")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetCalendario(int mes, int ano, DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Calendario>, int> tuple = await _agendaEntregaService.GetCalendario(mes, ano, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("produto")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetProdutos(Guid agendaGuid, DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Produto>, int> tuple = await _agendaEntregaService.GetProdutos(agendaGuid, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("entrega")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GerarEntrega(Guid pedidoEntregaGuid)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            int userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            int entregaId = await _agendaEntregaService.GerarEntrega(pedidoEntregaGuid, empresaId, userId);

            return Ok(entregaId);
        }
    }
}