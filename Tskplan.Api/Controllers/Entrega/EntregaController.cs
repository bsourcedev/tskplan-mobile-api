﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DTO = Tskplan.Models.Entrega.DTO;
using Tskplan.Models.Response;
using Microsoft.AspNetCore.Cors;
using Tskplan.Services.Entrega;
using Newtonsoft.Json;
using System.Security.Claims;

namespace Tskplan.Api.Controllers
{
    [Route("v1/entrega")]
    [Authorize]
    [ApiController]
    public class EntregaController : ControllerBase
    {
        private readonly IEntregaService _entregaService;
        private readonly ILogger<EntregaController> _logger;

        public EntregaController(IEntregaService entregaService, ILogger<EntregaController> logger)
        {
            _entregaService = entregaService ?? throw new ArgumentNullException(nameof(entregaService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #region Entrega

        [HttpGet]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Get(DateTime? lastSync, string data = "", bool processados = false)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Entrega>, int> tuple = await _entregaService.Get(empresaId, data is null? "": data, processados, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpPut]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Update([FromBody] DTO.Entrega entrega)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            int userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            List<DTO.Entrega> lista = new List<DTO.Entrega>();
            lista.Add(await _entregaService.Put(entrega, empresaId, userId));

            return Ok(new DefaultResponseData(lista, 1));
        }

        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Insert([FromBody] DTO.Entrega entrega)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            int userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            List<DTO.Entrega> lista = new List<DTO.Entrega>();
            lista.Add(await _entregaService.Post(entrega, empresaId, userId));

            return Ok(new DefaultResponseData(lista, 1));
        }
        #endregion

        #region Entrega Produto

        [HttpGet]
        [Route("produtos")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetProdutos(int entregaId, DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Produtos>, int> tuple = await _entregaService.GetProdutos(entregaId, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpPut]
        [Route("produtos/{entregaId}")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> UpdateProdutos([FromBody] DTO.Produtos produto)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            int userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            List<DTO.Produtos> lista = new List<DTO.Produtos>();
            lista.Add(await _entregaService.PutProdutos(produto, empresaId, userId));

            return Ok(new DefaultResponseData(lista, 1));
        }

        [HttpPost]
        [Route("produtos")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> InsertProdutos([FromBody] DTO.Produtos produto)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            int userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            List<DTO.Produtos> lista = new List<DTO.Produtos>();
            lista.Add(await _entregaService.PostProdutos(produto, empresaId, userId));

            return Ok(new DefaultResponseData(lista, 1));
        }

        #endregion

        #region Outros

        [HttpGet]
        [Route("produtoslista")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetProdutosLista(DateTime? lastSync, int fornecedorId = 0, string query = "")
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.ProdutosLista>, int> tuple = await _entregaService.GetProdutosLista(empresaId, fornecedorId, query, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("pedidos")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetPedidos(DateTime? lastSync, int fornecedorId = 0)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Pedido>, int> tuple = await _entregaService.GetPedidos(empresaId, fornecedorId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("fornecedor")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetFornecedores(DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Fornecedor>, int> tuple = await _entregaService.GetFornecedores(empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }


        #endregion
    }
}