﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DTO = Tskplan.Models.Checklist.DTO;
using Tskplan.Models.Response;
using Microsoft.AspNetCore.Cors;
using Tskplan.Services.Checklist;
using System.Security.Claims;

namespace Tskplan.Api.Controllers
{
    [Route("v1/checklist")]
    [Authorize]
    [ApiController]
    public class ChecklistController : ControllerBase
    {
        private readonly IChecklistService _checklistService;
        private readonly ILogger<ChecklistController> _logger;

        public ChecklistController(IChecklistService checklistService, ILogger<ChecklistController> logger)
        {
            _checklistService = checklistService ?? throw new ArgumentNullException(nameof(checklistService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Get(int obraCronogramaTarefaLocalId, int obraCronogramaTarefaChecklistId, DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Checklist>, int> tuple = await _checklistService.Get(obraCronogramaTarefaLocalId, obraCronogramaTarefaChecklistId, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpPut]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Update([FromBody] DTO.Checklist checklist)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);

            int checklistId = await _checklistService.Put(checklist, userId, empresaId);

            Tuple<IEnumerable<DTO.Checklist>, int> tuple = await _checklistService.GetById(checklistId, empresaId);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("lista")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetLista(int obraId, int obraLocalId, int tarefaId, DateTime? lastSync)
        {
            int userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value);
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.ChecklistLista>, int> tuple = await _checklistService.GetLista(obraId, obraLocalId, tarefaId,  empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }

        [HttpGet]
        [Route("arquivos")]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DefaultResponseData), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetArquivos(int ObraCronogramaTarefaLocalId, int ObraCronogramaTarefaCheckListId, DateTime? lastSync)
        {
            int empresaId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "EmpresaId").Value);
            Tuple<IEnumerable<DTO.Arquivo>, int> tuple = await _checklistService.Arquivos(ObraCronogramaTarefaLocalId, ObraCronogramaTarefaCheckListId, empresaId, lastSync);
            return Ok(new DefaultResponseData(tuple.Item1, tuple.Item2));
        }
    }
}