﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Tskplan.Api.Logger
{
    public class AppLogger : ILogger
    {
        private readonly string _nomeCategoria;
        private readonly Func<string, LogLevel, bool> _filtro;

        public AppLogger(string nomeCategoria, Func<string, LogLevel, bool> filtro)
        {
            _nomeCategoria = nomeCategoria;
            _filtro = filtro;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return (_filtro == null || _filtro(_nomeCategoria, logLevel));
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventoId, TState state, Exception exception, Func<TState, Exception, string> formato)
        {
            if (!IsEnabled(logLevel))
                return;

            if (formato == null)
                throw new ArgumentNullException(nameof(formato));

            var mensagem = formato(state, exception);
            if (string.IsNullOrEmpty(mensagem))
            {
                return;
            }

            if (exception != null)
                mensagem += $"\n{exception.ToString()}";

            EscreverTextoNoArquivo(mensagem);
        }

        private void EscreverTextoNoArquivo(string mensagem)
        {
            try
            {
                string caminhoArquivoLog = @".\Logs\";
                if (!Directory.Exists(caminhoArquivoLog))
                {
                    Directory.CreateDirectory(caminhoArquivoLog);
                }
                using (StreamWriter streamWriter = new StreamWriter(caminhoArquivoLog + $"log_{DateTime.Now.ToString("ddMMyyyyHH")}.txt", true))
                {
                    streamWriter.WriteLine(mensagem);
                    streamWriter.Close();
                }
            }
            catch
            {

            }
        }
    }
}
