﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tskplan.Models.Media.ViewModel;
using DTO = Tskplan.Models.Media.DTO;

namespace Tskplan.Services.Media
{
    public interface IMediaService
    {
        Task<DTO.Media> UpdateImage(MediaPost item);
    }
}
