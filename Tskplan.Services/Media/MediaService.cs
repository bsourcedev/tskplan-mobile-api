﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Media.DTO;
using Tskplan.Repositories.Media;
using Tskplan.Models.Media.ViewModel;
using Tskplan.Utils;
using Tskplan.Models.Configuration;

namespace Tskplan.Services.Media
{
    public class MediaService: IMediaService
    {
        private readonly IMediaRepository _mediaRepository;
        private readonly ILogger<MediaService> _logger;
        private readonly ConnectionStringsSettings _connectionStringsSettings;

        public MediaService(IMediaRepository mediaRepository, ILogger<MediaService> logger, IOptions<ConnectionStringsSettings> connectionStringsSettings)
        {
            _mediaRepository = mediaRepository ?? throw new ArgumentNullException(nameof(mediaRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionStringsSettings = connectionStringsSettings.Value;
        }

        public async Task<DTO.Media> UpdateImage(MediaPost item)
        {
            await SyncAzure(item);
            if (string.IsNullOrWhiteSpace(item.Url))
            {
                throw new Exception("Ocorreu um erro ao gravar a imagem");
            }
            else
            {
                return await _mediaRepository.UpdateImage(item);
            }
        }

        private async Task SyncAzure(MediaPost item)
        {
            try
            {
                item.Url = await AzureBlob.UploadBlobAsync(item.MemoryStream, item.Nome, _connectionStringsSettings);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
