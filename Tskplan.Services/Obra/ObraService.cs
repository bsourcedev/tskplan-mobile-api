﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Obra.DTO;
using Tskplan.Repositories.Obra;
using System.Linq;
using Tskplan.Utils;
using Newtonsoft.Json;

namespace Tskplan.Services.Obra
{
    public class ObraService : IObraService
    {
        private readonly IObraRepository _obraRepository;
        private readonly ILogger<ObraService> _logger;

        public ObraService(IObraRepository obraRepository, ILogger<ObraService> logger)
        {
            _obraRepository = obraRepository ?? throw new ArgumentNullException(nameof(obraRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<DTO.Entrega>, int>> Get(int empresaId, DateTime? lastSync)
        {
            return await _obraRepository.Get(empresaId, lastSync);
        }

        public async Task<Tuple<IEnumerable<DTO.ObraLocal>, int>> GetLocais(int obraId, int empresaId, DateTime? lastSync)
        {
            return await _obraRepository.GetLocais(obraId, empresaId, lastSync);
        }
    }
}
