﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Chat.DTO;
using Tskplan.Repositories.Chat;
using Tskplan.Models.Chat.DTO;

namespace Tskplan.Services.Chat
{
    public class ChatService : IChatService
    {
        private readonly IChatRepository _chatRepository;
        private readonly ILogger<ChatService> _logger;

        public ChatService(IChatRepository tarefaRepository, ILogger<ChatService> logger)
        {
            _chatRepository = tarefaRepository ?? throw new ArgumentNullException(nameof(tarefaRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<DTO.Chat>, int>> Get(int usuarioId, int empresaId, DateTime? lastSync)
        {
            return await _chatRepository.Get(usuarioId, empresaId, lastSync);
        }
        
        public async Task<Tuple<IEnumerable<DTO.Servers>, int>> GetServers(bool todosServers, int empresaId)
        {
            return await _chatRepository.GetServers(todosServers, empresaId);
        }

        public async Task PostMessage(MessageData message)
        {
            await _chatRepository.PostMessage(message);
        }

        public async Task<Tuple<IEnumerable<DTO.MessageData>, int>> ObtemMensagensDoCanal(Guid channel)
        {
            return await _chatRepository.ObtemMensagensDoCanal(channel);
        }

        public async Task<Tuple<IEnumerable<DTO.ChannelData>, int>> ObtemCanaisDoUsuario(string usuario)
        {
            return await _chatRepository.ObtemCanaisDoUsuario(usuario);
        }

        public async Task<Tuple<IEnumerable<DTO.ChannelData>, int>> ObtemCanais(int obraId, string pesquisa)
        {
            return await _chatRepository.ObtemCanais(obraId, pesquisa);
        }

        public async Task RegistrarToken(string token, int usuarioId)
        {
            await _chatRepository.RegistrarToken(token, usuarioId);
        }

        public async Task RegistrarUsuarioAoCanal(Guid channel, int usuarioId)
        {
            await _chatRepository.RegistrarUsuarioAoCanal(channel, usuarioId);
        }

    }
}