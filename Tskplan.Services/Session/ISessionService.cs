﻿using System.Threading.Tasks;
using DTO = Tskplan.Models.Session.DTO;
using Tskplan.Models.Session.ViewModel;

namespace Tskplan.Services.Session
{
    public interface ISessionService
    {
        Task<DTO.Session> Create(SessionPost item);
        void CreateToken(DTO.Session session);
    }
}
