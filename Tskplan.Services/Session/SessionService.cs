﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Tskplan.Models.Configuration;
using Tskplan.Repositories.Session;
using DTO = Tskplan.Models.Session.DTO;
using Tskplan.Models.Session.ViewModel;
using System.Security.Authentication;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Tskplan.Services.Session
{
    public class SessionService: ISessionService
    {
        private readonly ISessionRepository _sessionRepository;
        private readonly ILogger<SessionService> _logger;
        private readonly IOptions<AppSettings> _appSettings;

        public SessionService(ISessionRepository repository, ILogger<SessionService> logger, IOptions<AppSettings> appSettings)
        {
            _sessionRepository = repository ?? throw new ArgumentNullException(nameof(repository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _appSettings = appSettings ?? throw new ArgumentNullException(nameof(appSettings));
        }

        public async Task<DTO.Session> Create(SessionPost item)
        {
            if (string.IsNullOrWhiteSpace(item.User) || string.IsNullOrWhiteSpace(item.Password))
                return null;

            DTO.Session session = await _sessionRepository.Create(item);

            if (session == null || session.Id == 0)
                throw new AuthenticationException("Usuário não encontrado");

            if (!session.Senha.Equals(item.Password, StringComparison.InvariantCultureIgnoreCase))
                throw new AuthenticationException("Nome do Usuário ou Senha não Conferem!");
            //
            session.Senha = null;
            //
            CreateToken(session);
            //
            return session;
        }

        public void CreateToken(DTO.Session session)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(_appSettings.Value.Secret);
            DateTime expires = DateTime.UtcNow.AddYears(30).AddHours(12);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, session.Id.ToString()),
                    new Claim(ClaimTypes.NameIdentifier, session.Nome),
                    new Claim("EmpresaId", session.EmpresaId.ToString())
                }),
                Expires = expires,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            //
            session.Expires = expires.ToString("yyyy-MM-dd HH:mm:ss");
            session.Token = tokenHandler.WriteToken(token);
        }
    }
}
