﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tskplan.Models.Global.DTO;
using Tskplan.Repositories.Global;
using Tskplan.Services.Global;

namespace Tskplan.Services.Global
{
    public class GlobalService : IGlobalService
    {
        private readonly IGlobalRepository _globalRepository;
        private readonly ILogger<GlobalService> _logger;

        public GlobalService(IGlobalRepository globalRepository, ILogger<GlobalService> logger)
        {
            _globalRepository = globalRepository ?? throw new ArgumentNullException(nameof(globalRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<ModeloServico>, int>> GetModeloServico(int empresaId, DateTime? lastSync)
        {
            return await _globalRepository.GetModeloServico(empresaId, lastSync);

        }

        public async Task<Tuple<IEnumerable<Models.Global.DTO.Obra>, int>> GetObra(int empresaId, DateTime? lastSync)
        {
            return await _globalRepository.GetObra(empresaId, lastSync);
        }

        public async Task<Tuple<IEnumerable<UnidadeMedida>, int>> GetUnidadeMedida(int empresaId, DateTime? lastSync)
        {
            return await _globalRepository.GetUnidadeMedida(empresaId, lastSync);
        }
    }
}