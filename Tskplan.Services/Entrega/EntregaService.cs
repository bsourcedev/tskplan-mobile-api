﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Entrega.DTO;
using Tskplan.Repositories.Entrega;

namespace Tskplan.Services.Entrega
{
    public class EntregaService : IEntregaService
    {
        private readonly IEntregaRepository _entregaRepository;
        private readonly ILogger<EntregaService> _logger;

        public EntregaService(IEntregaRepository checklistRepository, ILogger<EntregaService> logger)
        {
            _entregaRepository = checklistRepository ?? throw new ArgumentNullException(nameof(checklistRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #region Entrega
        public async Task<Tuple<IEnumerable<DTO.Entrega>, int>> Get(int empresaId, string data, bool processados, DateTime? lastSync)
        {
            return await _entregaRepository.Get(empresaId, data, processados, lastSync);
        }

        public async Task<DTO.Entrega> Put(DTO.Entrega entrega, int empresaId, int usuarioId)
        {
            return await _entregaRepository.Put(entrega, empresaId, usuarioId);
        }

        public async Task<DTO.Entrega> Post(DTO.Entrega entrega, int empresaId, int usuarioId)
        {
            return await _entregaRepository.Post(entrega, empresaId, usuarioId);
        }
        #endregion

        #region Entrega Produto
        public async Task<Tuple<IEnumerable<DTO.Produtos>, int>> GetProdutos(int entregaId, int empresaId, DateTime? lastSync)
        {
            return await _entregaRepository.GetProdutos(entregaId, empresaId, lastSync);
        }

        public async Task<DTO.Produtos> PutProdutos(DTO.Produtos produto, int empresaId, int usuarioId)
        {
            return await _entregaRepository.PutProdutos(produto, empresaId, usuarioId);
        }

        public async Task<DTO.Produtos> PostProdutos(DTO.Produtos produto, int empresaId, int usuarioId)
        {
            return await _entregaRepository.PostProdutos(produto, empresaId, usuarioId);
        }
        #endregion

        #region Outros
                
        public async Task<Tuple<IEnumerable<DTO.ProdutosLista>, int>> GetProdutosLista(int empresaId, int fornecedorId, string query, DateTime? lastSync)
        {
            return await _entregaRepository.GetProdutosLista(empresaId, fornecedorId, query, lastSync);
        }

        public async Task<Tuple<IEnumerable<DTO.Pedido>, int>> GetPedidos(int empresaId, int fornecedorId, DateTime? lastSync)
        {
            return await _entregaRepository.GetPedidos(empresaId, fornecedorId, lastSync);
        }

        public async Task<Tuple<IEnumerable<DTO.Fornecedor>, int>> GetFornecedores(int empresaId, DateTime? lastSync)
        {
            return await _entregaRepository.GetFornecedores(empresaId, lastSync);
        }
        #endregion
    }
}