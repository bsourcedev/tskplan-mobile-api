﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Entrega.DTO;

namespace Tskplan.Services.Entrega
{
    public interface IEntregaService
    {
        Task<Tuple<IEnumerable<DTO.Entrega>, int>> Get(int empresaId, string data, bool processados, DateTime? lastSync);
        Task<DTO.Entrega> Put(DTO.Entrega produto, int empresaId, int usuarioId);
        Task<DTO.Entrega> Post(DTO.Entrega produto, int empresaId, int usuarioId);

        Task<Tuple<IEnumerable<DTO.ProdutosLista>, int>> GetProdutosLista(int empresaId, int fornecedorId, string query, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.Pedido>, int>> GetPedidos(int empresaId, int fornecedorId, DateTime? lastSync);
        Task<Tuple<IEnumerable<DTO.Fornecedor>, int>> GetFornecedores(int empresaId, DateTime? lastSync);

        Task<Tuple<IEnumerable<DTO.Produtos>, int>> GetProdutos(int entregaId, int empresaId, DateTime? lastSync);
        Task<DTO.Produtos> PutProdutos(DTO.Produtos produto, int empresaId, int usuarioId);
        Task<DTO.Produtos> PostProdutos(DTO.Produtos produto, int empresaId, int usuarioId);
    }
}
