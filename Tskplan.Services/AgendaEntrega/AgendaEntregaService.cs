﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DTO = Tskplan.Models.AgendaEntrega.DTO;
using Tskplan.Repositories.AgendaEntrega;

namespace Tskplan.Services.AgendaEntrega
{
    public class AgendaEntregaService : IAgendaEntregaService
    {
        private readonly IAgendaEntregaRepository _agendaEntregaRepository;
        private readonly ILogger<AgendaEntregaService> _logger;

        public AgendaEntregaService(IAgendaEntregaRepository agendaEntregaRepository, ILogger<AgendaEntregaService> logger)
        {
            _agendaEntregaRepository = agendaEntregaRepository ?? throw new ArgumentNullException(nameof(agendaEntregaRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<DTO.Calendario>, int>> GetCalendario(int mes, int ano, int empresaId, DateTime? lastSync)
        {
            return await _agendaEntregaRepository.GetCalendario(mes,ano, empresaId, lastSync);
        }

        public async Task<Tuple<IEnumerable<DTO.Produto>, int>> GetProdutos(Guid agendaGuid, int empresaId, DateTime? lastSync)
        {
            return await _agendaEntregaRepository.GetProdutos(agendaGuid, empresaId, lastSync);
        }

        public async Task<int> GerarEntrega(Guid pedidoEntregaGuid, int empresaId, int usuarioId)
        {
            return await _agendaEntregaRepository.GerarEntrega(pedidoEntregaGuid, empresaId, usuarioId);
        }
    }
}