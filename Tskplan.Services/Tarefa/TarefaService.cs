﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Tarefa.DTO;
using Tskplan.Repositories.Tarefa;

namespace Tskplan.Services.Tarefa
{
    public class TarefaService : ITarefaService
    {
        private readonly ITarefaRepository _tarefaRepository;
        private readonly ILogger<TarefaService> _logger;

        public TarefaService(ITarefaRepository checklistRepository, ILogger<TarefaService> logger)
        {
            _tarefaRepository = checklistRepository ?? throw new ArgumentNullException(nameof(checklistRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #region OS
        public async Task<Tuple<IEnumerable<DTO.OS>, int>> GetOS(Guid tarefaGuid, int empresaId, DateTime? lastSync)
        {
            return await _tarefaRepository.GetOS(tarefaGuid, empresaId, lastSync);
        }

        public async Task<Tuple<IEnumerable<DTO.OSLocais>, int>> GetOSLocais(int obraId, int tarefaId, int empresaId, DateTime? lastSync)
        {
            return await _tarefaRepository.GetOSLocais(obraId, tarefaId, empresaId, lastSync);
        }

        public async Task<Tuple<IEnumerable<DTO.OSLocais>, int>> GetOSLocaisAuxiliar(int empresaId, DateTime? lastSync)
        {
            return await _tarefaRepository.GetOSLocaisAuxiliar(empresaId, lastSync);
        }

        public async Task<Tuple<IEnumerable<DTO.TarefaListaLocais>, int>> GetListaLocais(int obraId, int obraLocalId, int empresaId, DateTime? lastSync)
        {
            return await _tarefaRepository.GetListaLocais(obraId, obraLocalId, empresaId, lastSync);
        }

        public async Task PostOS(DTO.OS os, int empresaId, int usuarioId)
        {
            await _tarefaRepository.PostOS(os, empresaId, usuarioId);
        }

        public async Task PutOS(DTO.OS os, int empresaId, int usuarioId)
        {
            await _tarefaRepository.PutOS(os, empresaId, usuarioId);
        }
        #endregion

        #region Tarefa

        public async Task<Tuple<IEnumerable<DTO.TarefaLista>, int>> GetLista(int obraId, int empresaId, DateTime? lastSync, int? statusId, string? query)
        {
            return await _tarefaRepository.GetLista(obraId, empresaId, lastSync, statusId, query);
        }

        public async Task<Tuple<IEnumerable<DTO.Tarefa>, int>> Get(Guid tarefaGuid, int empresaId)
        {
            return await _tarefaRepository.Get(tarefaGuid, empresaId);
        }

        public async Task Post(DTO.Tarefa tarefa, int empresaId, int usuarioId)
        {
            await _tarefaRepository.Post(tarefa, empresaId, usuarioId);
        }

        public async Task Put(DTO.Tarefa tarefa, int empresaId, int usuarioId)
        {
            await _tarefaRepository.Put(tarefa, empresaId, usuarioId);
        }

        #endregion

        public async Task<Tuple<IEnumerable<DTO.Arquivo>, int>> Arquivos(Guid tarefaGuid, int empresaId, DateTime? lastSync)
        {
            return await _tarefaRepository.Arquivos(tarefaGuid, empresaId, lastSync);
        }
    }
}