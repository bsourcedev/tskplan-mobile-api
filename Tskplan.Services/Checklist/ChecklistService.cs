﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DTO = Tskplan.Models.Checklist.DTO;
using Tskplan.Repositories.Checklist;

namespace Tskplan.Services.Checklist
{
    public class ChecklistService : IChecklistService
    {
        private readonly IChecklistRepository _checklistRepository;
        private readonly ILogger<ChecklistService> _logger;

        public ChecklistService(IChecklistRepository checklistRepository, ILogger<ChecklistService> logger)
        {
            _checklistRepository = checklistRepository ?? throw new ArgumentNullException(nameof(checklistRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Tuple<IEnumerable<DTO.ChecklistLista>, int>> GetLista(int obraId, int obraLocalId, int tarefaId, int empresaId, DateTime? lastSync)
        {
            return await _checklistRepository.GetLista(obraId, obraLocalId, tarefaId, empresaId, lastSync);
        }

        public async Task<Tuple<IEnumerable<DTO.Checklist>, int>> Get(int obraCronogramaTarefaLocalId, int obraCronogramaTarefaChecklistId, int empresaId, DateTime? lastSync)
        {
            return await _checklistRepository.Get(obraCronogramaTarefaLocalId, obraCronogramaTarefaChecklistId, empresaId, lastSync);
        }

        public async Task<Tuple<IEnumerable<DTO.Checklist>, int>> GetById(int OrdemServicoCheckListId, int empresaId)
        {
            return await _checklistRepository.GetById(OrdemServicoCheckListId, empresaId);
        }

        public async Task<int> Put(DTO.Checklist checklist, int usuarioId, int empresaId)
        {
            return await _checklistRepository.Put(checklist, usuarioId, empresaId);
        }

        public async Task<Tuple<IEnumerable<DTO.Arquivo>, int>> Arquivos(int ObraCronogramaTarefaLocalId, int ObraCronogramaTarefaCheckListId, int empresaId, DateTime? lastSync)
        {
            return await _checklistRepository.Arquivos(ObraCronogramaTarefaLocalId, ObraCronogramaTarefaCheckListId, empresaId, lastSync);
        }
    }
}