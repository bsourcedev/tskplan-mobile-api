﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;

namespace Tskplan.Data
{
    public interface IDapperDatabaseHelper
    {
        /// <summary>
        /// Método responsável por criar a conexão com o Banco de dados em questão
        /// </summary>
        /// <param name="databaseName">Parametro utilizado nas conexões do MySql para saber qual database será utilizado, sendo padrão o "cartorio"</param>
        void CreateConnection();
        /// <summary>
        /// Método responsável por criar uma transação com a conexão criada, caso o parametro seja true
        /// </summary>
        /// <param name="useTransaction">Parametro utilizado para saber se precisa criar a transação</param>
        void CreateTransaction(bool useTransaction = true);
        /// <summary>
        /// Método responsável por abrir a conexão criada anteriormente, caso a mesma esteja fechada e exista
        /// </summary>
        void OpenConnection();
        /// <summary>
        /// Método responsável por realizar o commit e o dispose (destroy) da transação, caso ainda exista uma transação
        /// </summary>
        void CommitTransaction();
        /// <summary>
        /// Método utilizado quando é apresentado um erro, o mesmo irá realizar o dispose (destroy) da transação, não commitando a mesma
        /// </summary>
        void RollbackTransaction();
        /// <summary>
        /// Método responsável por realizar o close da conexão para não utilização em mais nenhum caso
        /// </summary>
        void CloseConnection();

        #region QueryAll
        //-----------------------------------------------------------------------------------------------------------------------------------//
        /// <summary>
        /// Método responsável por realizar consulta de N items no banco de dados
        /// Caso deseje realizar N ações utilizando a mesma transação, será necessário utilizar os métodos (CreateConnection, CreateTransaction, CommitTransaction, CloseConnection) no Repository que chamará a ação abaixo
        /// Case deseje realizar cada ação com ou sem uma transação, não utilizar os métodos de criação de conexão, e passar o valor para a variavel useTransaction
        /// </summary>
        /// <typeparam name="T">Objeto que será retornado para a tela</typeparam>
        /// <param name="script">Script contendo o SELECT, UPDATE, DELETE, INSERT ou qualquer outro commando para o banco de dados</param>
        /// <param name="useTransaction">Variavel para informar se será utilizado a transação ou não</param>
        /// <param name="dParams">Parametros dinamicos a serem utilizados pelo script</param>
        /// <param name="totalOutParam">Nome do campo que será armazenado o total</param>
        /// <param name="useProcedure">Variavel para definir se o script é um texto, ou se é apenas o nome de uma procedure para ser executada</param>
        /// <param name="databaseName">Nome do database que será utilizado na criação da conexão</param>
        /// <param name="preTotal">Valor de total de registros que a consulta vai retornar.</param>
        /// <returns>Retorna um Tuple com os items retornados do banco de dados</returns>
        Task<Tuple<IEnumerable<T>, int>> QueryAll<T>(string script, bool useTransaction = false, DynamicParameters dParams = null, string totalOutParam = "", bool useProcedure = false, int preTotal = 0);
        #endregion

        #region QueryOne
        //-----------------------------------------------------------------------------------------------------------------------------------//
        /// <summary>
        /// Método responsável por realizar a consulta de 1 item no banco de dados
        /// Caso deseje realizar N ações utilizando a mesma transação, será necessário utilizar os métodos (CreateConnection, CreateTransaction, CommitTransaction, CloseConnection) no Repository que chamará a ação abaixo
        /// Case deseje realizar cada ação com ou sem uma transação, não utilizar os métodos de criação de conexão, e passar o valor para a variavel useTransaction
        /// </summary>
        /// <typeparam name="T">Objeto que será retornado para a tela</typeparam>
        /// <param name="script">Script contendo o SELECT, UPDATE, DELETE, INSERT ou qualquer outro commando para o banco de dados</param>
        /// <param name="useTransaction">Variavel para informar se será utilizado a transação ou não</param>
        /// <param name="dParams">Parametros dinamicos a serem utilizados pelo script</param>
        /// <param name="useProcedure">Variavel para definir se o script é um texto, ou se é apenas o nome de uma procedure para ser executada</param>
        /// <param name="databaseName">Nome do database que será utilizado na criação da conexão</param>
        /// <returns>Retorna o item que foi encontrado no banco de dados</returns>
        Task<T> QueryOne<T>(string script, bool useTransaction = false, DynamicParameters dParams = null, bool useProcedure = false);
        #endregion

        #region Execute
        //-----------------------------------------------------------------------------------------------------------------------------------//
        /// <summary>
        /// Método responsável por executar uma ação, sem retorno nenhum para o método que o chamou
        /// Caso deseje realizar N ações utilizando a mesma transação, será necessário utilizar os métodos (CreateConnection, CreateTransaction, CommitTransaction, CloseConnection) no Repository que chamará a ação abaixo
        /// Case deseje realizar cada ação com ou sem uma transação, não utilizar os métodos de criação de conexão, e passar o valor para a variavel useTransaction
        /// </summary>
        /// <param name="script">Script contendo o SELECT, UPDATE, DELETE, INSERT ou qualquer outro commando para o banco de dados</param>
        /// <param name="useTransaction">Variavel para informar se será utilizado a transação ou não</param>
        /// <param name="dParams">Parametros dinamicos a serem utilizados pelo script</param>
        /// <param name="useProcedure">Variavel para definir se o script é um texto, ou se é apenas o nome de uma procedure para ser executada</param>
        /// <param name="databaseName">Nome do database que será utilizado na criação da conexão</param>
        Task Execute(string script, bool useTransaction = false, DynamicParameters dParams = null, bool useProcedure = false);
        #endregion

        #region ExecuteScalar
        //-----------------------------------------------------------------------------------------------------------------------------------//
        /// <summary>
        /// Método responsável por executar uma ação, retornando o dado no modelo passado como parametro
        /// Caso deseje realizar N ações utilizando a mesma transação, será necessário utilizar os métodos (CreateConnection, CreateTransaction, CommitTransaction, CloseConnection) no Repository que chamará a ação abaixo
        /// Case deseje realizar cada ação com ou sem uma transação, não utilizar os métodos de criação de conexão, e passar o valor para a variavel useTransaction
        /// </summary>
        /// <typeparam name="T">Objeto que será retornado pela função</typeparam>
        /// <param name="script">Script contendo o SELECT, UPDATE, DELETE, INSERT ou qualquer outro commando para o banco de dados</param>
        /// <param name="useTransaction">Variavel para informar se será utilizado a transação ou não</param>
        /// <param name="dParams">Parametros dinamicos a serem utilizados pelo script</param>
        /// <param name="useProcedure">Variavel para definir se o script é um texto, ou se é apenas o nome de uma procedure para ser executada</param>
        /// <param name="databaseName">Nome do database que será utilizado na criação da conexão</param>
        /// <returns>Retorna o item que foi encontrado no banco de dados</returns>
        Task<T> ExecuteScalar<T>(string script, bool useTransaction = false, DynamicParameters dParams = null, bool useProcedure = false);
        #endregion
    }
}
