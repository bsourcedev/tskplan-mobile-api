﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Tskplan.Models.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace Tskplan.Data
{
    public class SqlDapperDatabaseHelper: IDapperDatabaseHelper
    {
        /// <summary>
        /// PARA MELHOR ENTENDIMENTO DOS MÉTODOS ABAIXO, FOI ADICIONADO COMENTÁRIOS NA INTERFACE IDAPPERDATABASEHELPER
        /// </summary>
        private readonly ConnectionStringsSettings _connectionStringsSettings;
        private SqlConnection _sqlConnection = null;
        private SqlTransaction _sqlTransaction = null;
        private bool _existConnection = false;

        public SqlDapperDatabaseHelper(IOptions<ConnectionStringsSettings> connectionStringsSettings)
        {
            _connectionStringsSettings = connectionStringsSettings.Value;
            _sqlConnection = null;
            _sqlTransaction = null;
            _existConnection = false;
        }

        public void CreateConnection()
        {
            _sqlConnection = new SqlConnection(_connectionStringsSettings.SqlServerDefault);
            OpenConnection();
        }

        public void CreateTransaction(bool useTransaction = true)
        {
            if (useTransaction)
            {
                OpenConnection();
                _sqlTransaction = _sqlConnection.BeginTransaction();
            }
        }

        public void OpenConnection()
        {
            if (_sqlConnection != null)
            {
                if (_sqlConnection.State.Equals(ConnectionState.Closed))
                {
                    _sqlConnection.Open();
                }
            }
        }

        public void CommitTransaction()
        {
            if (_sqlTransaction != null)
            {
                _sqlTransaction.Commit();
                _sqlTransaction.Dispose();
                _sqlTransaction = null;
            }
        }

        public void RollbackTransaction()
        {
            if (_sqlTransaction != null)
            {
                _sqlTransaction.Dispose();
                _sqlTransaction = null;
            }
        }

        public void CloseConnection()
        {
            _sqlConnection.Close();
            _sqlConnection = null;
        }

        public async Task<Tuple<IEnumerable<T>, int>> QueryAll<T>(string script, bool useTransaction = false, DynamicParameters dParams = null, string totalOutParam = "", bool useProcedure = false, int preTotal = 0)
        {
            _existConnection = _sqlConnection != null;
            if (!_existConnection)
            {
                CreateConnection();
                CreateTransaction(useTransaction);
            }
            Tuple<IEnumerable<T>, int> retorno = await QueryAll<T>(script, dParams, totalOutParam, useProcedure, preTotal);
            if (!_existConnection)
            {
                CommitTransaction();
                CloseConnection();
            }
            return retorno;
        }

        private async Task<Tuple<IEnumerable<T>, int>> QueryAll<T>(string script, DynamicParameters dParams, string totalOutParam, bool useProcedure, int preTotal)
        {
            IEnumerable<T> items = null;
            int total = 0;
            try
            {
                items = await _sqlConnection.QueryAsync<T>(
                    script,
                    param: dParams,
                    transaction: _sqlTransaction,
                    commandTimeout: 120,
                    commandType: !useProcedure ? CommandType.Text : CommandType.StoredProcedure
                    );
                //
                if (dParams != null && !string.IsNullOrWhiteSpace(totalOutParam))
                {
                    total = dParams.Get<int>(totalOutParam);
                }
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
            return new Tuple<IEnumerable<T>, int>(items, total);
        }

        public async Task<T> QueryOne<T>(string script, bool useTransaction = false, DynamicParameters dParams = null, bool useProcedure = false)
        {
            _existConnection = _sqlConnection != null;
            if (!_existConnection)
            {
                CreateConnection();
                CreateTransaction(useTransaction);
            }
            T retorno = await QueryOne<T>(script, dParams, useProcedure);
            if (!_existConnection)
            {
                CommitTransaction();
                CloseConnection();
            }
            return retorno;
        }

        private async Task<T> QueryOne<T>(string script, DynamicParameters dParams, bool useProcedure)
        {
            T item = default(T);
            try
            {
                item = await _sqlConnection.QueryFirstOrDefaultAsync<T>(
                    script,
                    param: dParams,
                    transaction: _sqlTransaction,
                    commandTimeout: 120,
                    commandType: !useProcedure ? CommandType.Text : CommandType.StoredProcedure
                    );
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
            return item;
        }

        public async Task Execute(string script, bool useTransaction = false, DynamicParameters dParams = null, bool useProcedure = false)
        {
            _existConnection = _sqlConnection != null;
            if (!_existConnection)
            {
                CreateConnection();
                CreateTransaction(useTransaction);
            }
            await Execute(script, dParams, useProcedure);
            if (!_existConnection)
            {
                CommitTransaction();
                CloseConnection();
            }
        }

        private async Task Execute(string script, DynamicParameters dParams, bool useProcedure)
        {
            try
            {
                await _sqlConnection.ExecuteAsync(
                    script,
                    param: dParams,
                    transaction: _sqlTransaction,
                    commandTimeout: 120,
                    commandType: !useProcedure ? CommandType.Text : CommandType.StoredProcedure
                    );
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }

        public async Task<T> ExecuteScalar<T>(string script, bool useTransaction = false, DynamicParameters dParams = null, bool useProcedure = false)
        {
            _existConnection = _sqlConnection != null;
            if (!_existConnection)
            {
                CreateConnection();
                CreateTransaction(useTransaction);
            }
            T retorno = await ExecuteScalar<T>(script, dParams, useProcedure);
            if (!_existConnection)
            {
                CommitTransaction();
                CloseConnection();
            }
            return retorno;
        }

        private async Task<T> ExecuteScalar<T>(string script, DynamicParameters dParams, bool useProcedure)
        {
            T variable = default(T);
            try
            {
                variable = await _sqlConnection.ExecuteScalarAsync<T>(
                    script,
                    param: dParams,
                    transaction: _sqlTransaction,
                    commandTimeout: 120,
                    commandType: !useProcedure ? CommandType.Text : CommandType.StoredProcedure
                    );
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
            return variable;
        }
    }
}
