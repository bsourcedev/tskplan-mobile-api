﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tskplan.Models.Obra.DTO
{
    public class Entrega : Base
    {
        public int ObraId { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public int Pendentes { get; set; }
        public int Rejeitados { get; set; }
        public int Aprovados { get; set; }
    }
}
