﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tskplan.Models.Obra.DTO
{
    public class ObraLocal : Base
    {
        public int ObraLocalId { get; set; }
        public int ObraId { get; set; }
        public string Caminho { get; set; }
        public string Nome { get; set; }
        public int PaiId { get; set; }
    }
}
