﻿using System;

namespace Tskplan.Models.Global.DTO
{
    public class ModeloServico: Base
    {
        public int ModeloServicoId { get; set; }
        public string Nome { get; set; }
        public int GrupoCompraId { get; set; }
    }
}
