﻿using System;

namespace Tskplan.Models.Global.DTO
{
    public class UnidadeMedida: Base
    {
        public int UnidadeMedidaId { get; set; }
        public string Nome { get; set; }
    }
}
