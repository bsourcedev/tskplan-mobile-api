﻿using System;

namespace Tskplan.Models.Global.DTO
{
    public class Obra: Base
    {
        public int ObraId { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
    }
}
