﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tskplan.Models.Entrega.DTO
{
    public class Entrega : Base
    {
        public int EntregaId { get; set; }
        public int PedidoCompraId { get; set; }
        public int ObraId { get; set; }
        public string Obra { get; set; }
        public int FornecedorId  { get; set; }
        public string Fornecedor { get; set; }
        public DateTime Data { get; set; }
        public bool Processado { get; set; }
        public string Observacao { get; set; }
        public string NotaFiscal { get; set; }
        public string Serie { get; set; }
    }
}
