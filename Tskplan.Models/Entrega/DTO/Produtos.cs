﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tskplan.Models.Entrega.DTO
{
    public class Produtos : Base
    {
        public int EntregaProdutoId { get; set; }
        public int EntregaId { get; set; }
        public int ProdutoId { get; set; }
        public string Produto { get; set; }
        public decimal Quantidade{ get; set; }
        public bool Pedido { get; set; }
        public string Observacao { get; set; }
    }
}
