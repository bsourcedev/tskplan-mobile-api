﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tskplan.Models.Entrega.DTO
{
    public class ProdutosLista : Base
    {
        public int ProdutoId { get; set; }
        public string Produto { get; set; }
    }
}
