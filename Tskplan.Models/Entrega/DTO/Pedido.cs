﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tskplan.Models.Entrega.DTO
{
    public class Pedido : Base
    {
        public int PedidoCompraId { get; set; }
        public int FornecedorId { get; set; }
        public string Fornecedor { get; set; }
        public DateTime Data { get; set; }
        public decimal Porcentagem { get; set; }
        public int ObraId { get; set; }
        public string Obra { get; set; }
        public string ObraEndereco { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
    }
}
