﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tskplan.Models.Entrega.DTO
{
    public class Fornecedor : Base
    {
        public int FornecedorId { get; set; }
        public string Nome { get; set; }
    }
}
