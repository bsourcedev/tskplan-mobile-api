﻿using System;

namespace Tskplan.Models
{
    public class Base
    {
        public Guid guid { get; set; } = new Guid();
        public DateTime syncDate { get; set; } = DateTime.Now;
        public int isSync { get; set; } = 1;
        public int isCreated { get; set; } = 1;
        public int isDeleted { get; set; } = 0;
    }
}
