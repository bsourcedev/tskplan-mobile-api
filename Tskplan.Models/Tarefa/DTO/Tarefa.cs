﻿using System;

namespace Tskplan.Models.Tarefa.DTO
{
    public class Tarefa : Base
    {
        public int TarefaId { get; set; }
        public string Nome { get; set; }
        public decimal Quantidade { get; set; }
        public int ObraId { get; set; }
        public string Obra { get; set; }
        public int ModeloServicoId { get; set; }
        public string ModeloServico { get; set; }
        public int UnidadeMedidaId { get; set; }
        public string UnidadeMedida { get; set; }
        public int GrupoCompraId { get; set; }
        public string GrupoCompra { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int StatusId { get; set; }
        public decimal Porcentagem { get; set; }
        public string Observacao { get; set; }
        public string Icone { get; set; }
        public int OSAberta { get; set; }
        public int OSAndamento { get; set; }
        public int OSConcluidas { get; set; }
    }
}
