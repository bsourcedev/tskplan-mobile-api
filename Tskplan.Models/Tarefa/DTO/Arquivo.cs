﻿using System;

namespace Tskplan.Models.Tarefa.DTO
{
    public class Arquivo : Base
    {
        public int EmpresaId { get; set; }
        public int ObraCronogramaTarefaId { get; set; }
        public Guid TarefaGuid { get; set; }
        public string Nome{ get; set; }
        public string Caminho{ get; set; }
        public string Extensao{ get; set; }
        public DateTime DataHoraAlteracao{ get; set; }
    }
}
