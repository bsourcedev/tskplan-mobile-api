﻿namespace Tskplan.Models.Tarefa.DTO
{
    public class TarefaListaLocais : Base
    {
        public int ObraCronogramaTarefaId { get; set; }
        public int ObraCronogramaTarefaLocalId { get; set; }
        public int ObraId { get; set; }
        public int ObraLocalId { get; set; }
        public string Tarefa { get; set; }
    }
}
