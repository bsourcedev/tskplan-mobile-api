﻿using System;

namespace Tskplan.Models.Tarefa.DTO
{
    public class TarefaLista : Base
    {
        public int TarefaId { get; set; }
        public string Nome { get; set; }
        public int ObraId { get; set; }
        public string Obra { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public string Icone { get; set; }
        public int StatusId { get; set; }
        public decimal Porcentagem { get; set; }
    }
}
