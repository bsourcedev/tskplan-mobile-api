﻿namespace Tskplan.Models.Tarefa.DTO
{
    public class OSLocais : Base
    {
        public int ObraLocalId { get; set; }
        public int ObraId { get; set; }
        public string Caminho { get; set; }
        public string Nome { get; set; }
        public int PaiId { get; set; }
        public int TarefaId { get; set; }
    }
}
