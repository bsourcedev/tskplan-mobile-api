﻿using System;

namespace Tskplan.Models.Tarefa.DTO
{
    public class OS : Base
    {
        public int ObraCronogramaTarefaLocalId { get; set; }
        public int ObraCronogramaTarefaId { get; set; }
        public int ObraLocalId { get; set; }
        public string Local { get; set; }
        public int Andamento { get; set; }
        public bool Checked { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public Guid? TarefaGuid { get; set; }
    }
}

