﻿using System;

namespace Tskplan.Models.Checklist.DTO
{
    public class Checklist : Base
    {
        public string Id { get; set; }
        public int ObraCronogramaTarefaLocalId { get; set; }
        public int ObraCronogramaTarefaId { get; set; }
        public int ObraCronogramaTarefaCheckListId { get; set; }
        public int OrdemServicoCheckListId { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Motivo { get; set; }
        public Guid MotivoImagemGuid { get; set; }
        public string MotivoImagemURL { get; set; }
        public DateTime DataInspecao { get; set; }
        public string QuantoConferir { get; set; }
        public bool Visual { get; set; }
        public bool Trena { get; set; }
        public bool Projeto { get; set; }
        public bool Nivel { get; set; }
        public bool Prumo { get; set; }
        public string InformacaoComplementar { get; set; }
        public string Aceitacao { get; set; }
        public string Observacao { get; set; }
        public int StatusId { get; set; }
        public DateTime DataHoraAlteracao { get; set; }
    }
}
