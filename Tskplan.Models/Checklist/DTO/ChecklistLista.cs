﻿using System;

namespace Tskplan.Models.Checklist.DTO
{
    public class ChecklistLista : Base
    {
        public string Id { get; set; }
        public int ObraCronogramaTarefaLocalId { get; set; }
        public int ObraCronogramaTarefaId { get; set; }
        public int ObraCronogramaTarefaCheckListId { get; set; }
        public int OrdemServicoCheckListId { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public int StatusId { get; set; }
        public DateTime DataHoraAlteracao { get; set; } 

        public int ObraId { get; set; }
        public int ObraLocalId { get; set; }
        public int TarefaId { get; set; }
        public Guid TarefaGuid{ get; set; }
    }
}
