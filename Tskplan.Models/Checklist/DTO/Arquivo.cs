﻿using System;

namespace Tskplan.Models.Checklist.DTO
{
    public class Arquivo : Base
    {
        public int EmpresaId { get; set; }
        public int ObraCronogramaTarefaLocalId { get; set; }
        public int ObraCronogramaTarefaCheckListId { get; set; }
        public string Nome{ get; set; }
        public string Caminho{ get; set; }
        public string Extensao{ get; set; }
        public DateTime DataHoraAlteracao{ get; set; }
    }
}
