﻿using System;

namespace Tskplan.Models.AgendaEntrega.DTO
{
    public class Calendario : Base
    {
        public int PedidoCompraEntregaId { get; set; }
        public int PedidoCompraId { get; set; }
        public DateTime DataEntrega { get; set; }
        public string Observacao { get; set; }
        public string Fornecedor { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public int ObraId { get; set; }
        public string Obra { get; set; }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public DateTime DataHoraAlteracao{ get; set; }
    }
}
