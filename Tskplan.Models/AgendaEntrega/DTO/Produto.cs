﻿using System;

namespace Tskplan.Models.AgendaEntrega.DTO
{
    public class Produto : Base
    {
        public int PedidoCompraEntregaItemId { get; set; }
        public int PedidoCompraEntregaId { get; set; }
        public int ProdutoId { get; set; }
        public string Nome { get; set; }
        public Guid AgendaGuid { get; set; }
        public decimal Quantidade { get; set; }
        public DateTime DataHoraAlteracao{ get; set; }
    }
}
