﻿namespace Tskplan.Models.Session.ViewModel
{
    public class SessionPost
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}
