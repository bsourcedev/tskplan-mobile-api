﻿namespace Tskplan.Models.Session.DTO
{
    public class Session
    {
        public int EmpresaId { get; set; }
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Expires { get; set; }
        public string Token { get; set; }
    }
}
