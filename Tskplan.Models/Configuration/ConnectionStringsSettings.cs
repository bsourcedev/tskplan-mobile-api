﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tskplan.Models.Configuration
{
    public class ConnectionStringsSettings
    {
        public string SqlServerDefault { get; set; }
        public string AzureDefault { get; set; }
    }
}
