﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tskplan.Models.Media.DTO
{
    public class Media
    {
        public int EmpresaId { get; set; }
        public int MidiaId { get; set; }
        public Guid Guid { get; set; }
        public string Url { get; set; }
        public string Plataforma { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Extensao { get; set; }
        public int UsuarioIdInclusao { get; set; }
        public DateTime DataHoraInclusao { get; set; }
    }
}
