﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Tskplan.Models.Media.ViewModel
{
    public class MediaPost
    {
        public int EmpresaId { get; set; }
        public int UsuarioId { get; set; }
        public int MediaId { get; set; }
        public string Base64 { get; set; }
        public Guid Guid { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Url { get; set; }
        public string Nome { get; set; }
        public string Extensao { get; set; }
        public byte[] Bytes => Convert.FromBase64String(Base64.IndexOf("base64") > -1 ? Base64.Substring(Base64.IndexOf("base64") + 7) : Base64);
        public MemoryStream MemoryStream => new MemoryStream(Bytes);
    }
}
