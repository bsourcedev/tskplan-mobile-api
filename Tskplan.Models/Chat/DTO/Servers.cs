﻿using System;

namespace Tskplan.Models.Chat.DTO
{
    public class Servers : Base
    {
        public int ObraId { get; set; }
        public Guid Id { get; set; }
        public string ServerName { get; set; }
        public string ShortName { get; set; }
        public string IconUrl { get; set; }
    }
}
