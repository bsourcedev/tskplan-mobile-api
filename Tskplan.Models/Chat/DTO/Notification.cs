﻿using System;

namespace Tskplan.Models.Chat.DTO
{
    public class Notification : Base
    {
        public NotificationData notification { get; set; }
        public string to { get; set; }
    }
}
