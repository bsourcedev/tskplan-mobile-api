﻿using System;

namespace Tskplan.Models.Chat.DTO
{
    public class ChannelData : Base
    {
        public Guid Channel { get; set; }
        public string ChannelName { get; set; }
        public Guid Server { get; set; }
        public string ServerName { get; set; }
        public string ServerShortName { get; set; }
    }
}
