﻿using System;

namespace Tskplan.Models.Chat.DTO
{
    public class MessageData : Base
    {
        public Guid? messageId { get; set; }
        public string messageType { get; set; }
        public string messageText { get; set; }
        public DateTime senderCreationDate { get; set; }
        public int status { get; set; }
        public string urlItem { get; set; }
        public Guid? channel { get; set; }
        public string channelName { get; set; }
        public Guid ?server { get; set; }
        public string serverName { get; set; }
        public string serverShortName { get; set; }
        public string user { get; set; }
        public string userName { get; set; }
        public Guid? responseGuid { get; set; }
    }
}
