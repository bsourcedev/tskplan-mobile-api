﻿using System;

namespace Tskplan.Models.Chat.DTO
{
    public class Chat : Base
    {
        public int EmpresaId { get; set; }
        public int TarefaChatId { get; set; }
        public int TarefaId { get; set; }
        public string Titulo { get; set; }
        public string Obra { get; set; }
        public bool Grupo { get; set; }
        public string UltimaMensagem { get; set; }
        public string UltimaMensagemUsuario { get; set; }
        public DateTime UltimaMensagemData { get; set; }
        public DateTime DataHoraInclusao { get; set; }
    }
}
