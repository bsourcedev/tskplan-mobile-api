﻿using System;

namespace Tskplan.Models.Chat.DTO
{
    public class NotificationData : Base
    {
        public string title { get; set; }
        public string body { get; set; }
        public string icon { get; set; }
        public string click_action { get; set; }
    }
}
