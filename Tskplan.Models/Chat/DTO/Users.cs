﻿using System;

namespace Tskplan.Models.Chat.DTO
{
    public class Users : Base
    {
        public string id { get; set; }
        public string name { get; set; }
        public string login { get; set; }
        public string token { get; set; }
        public Guid session { get; set; }
    }
}
