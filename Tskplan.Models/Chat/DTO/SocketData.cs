﻿using System;
using System.Collections.Generic;

namespace Tskplan.Models.Chat.DTO
{
    public class SocketData : Base
    {
        public List<string> destinatarios { get; set; }
        public MessageData mensagem { get; set; }
    }
}
