﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Tskplan.Models.Response
{
    public class ErrorResponseData
    {
        [JsonProperty("statusCode")]
        public int StatusCode { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("errorCode")]
        public int ErrorCode { get; set; }
        [JsonProperty("errorNumber")]
        public int ErrorNumber { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("exceptionDetails")]
        public Exception ExceptionDetails { get; set; }
        [JsonProperty("otherDetails")]
        public object OtherDetails { get; set; }

        public ErrorResponseData()
        {
        }
    }
}
