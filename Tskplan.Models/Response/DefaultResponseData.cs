﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tskplan.Models.Response
{
    public class DefaultResponseData
    {
        [JsonProperty("data")]
        public IEnumerable<object> Data { get; set; }
        [JsonProperty("total")]
        public int Total { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }

        public DefaultResponseData()
        {
        }

        public DefaultResponseData(IEnumerable<object> data, int total = 0) : this()
        {
            Data = data;
            Total = total;
        }
    }
}
